#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QTextStream>

#include "ConfigHandler.h"

using namespace catec;

ConfigHandler::ConfigHandler(QWidget *parent, Ui::MainWindow *ui) : QWidget(parent)
{
   //QLOG_TRACE() << "ConfigHandler::ConfigHandler()";

   _ui = ui;

   connect(_ui->config_saveB, SIGNAL(clicked()), this, SLOT(saveConfig()));
   reset();
}

void ConfigHandler::saveConfig()
{
   //QLOG_TRACE() << "ConfigHandler::saveConfig()";

   SystemSettings *system_settings = SystemSettings::getInstance();

   QString save_settings_text = _ui->configPTE->toPlainText();
   QFile file(system_settings->settings->fileName());
   file.open(QIODevice::WriteOnly);
   file.resize(0);
   file.write(save_settings_text.toUtf8());
   file.close();

   reset();
}

void ConfigHandler::reset()
{
   //QLOG_TRACE() << "ConfigHandler::reset()";

   SystemSettings *system_settings = SystemSettings::getInstance();

   //QDir config_file_path(QCoreApplication::applicationDirPath());
   //config_file_path.cd(".\\config");
   QString config_file_path = ".\\config";
   //QDir config_file_path(QCoreApplication::applicationDirPath());
   //config_file_path.cd("../share/virtual_drone_box");
#ifdef DEBUG
   QFileInfo config_file_full_path(config_file_path + "/system_config_debug.ini");
#else
   QFileInfo config_file_full_path(config_file_path + "/system_config.ini");
#endif
   if (!config_file_full_path.exists())
      throw std::runtime_error("ERROR: Path to system_config.ini does not exists <" +
                               config_file_full_path.filePath().toStdString() + ">");
   system_settings->settings = new QSettings(config_file_full_path.filePath(), QSettings::IniFormat);

   //QLOG_DEBUG() << "System Settings file path: " << system_settings->settings->fileName();

   // TODO: try catch?
   QFile settings_file(system_settings->settings->fileName());
   settings_file.open(QFile::ReadOnly | QFile::Text);
   QString settings_file_text = QTextStream(&settings_file).readAll();
   settings_file.close();

   //_ui->configPTE->setPlainText(settings_file_text);

   ConfigResult result = checkConfig();
}

ConfigResult ConfigHandler::checkConfig()
{
   //QLOG_TRACE() << "ConfigHandler::checkConfig()";

   SystemSettings *system_settings = SystemSettings::getInstance();

   ConfigResult result = {true, QStringList()};

   // [FILES]
   auto checkFile{[system_settings, &result](const QString &group, const QString &key) {
      if (system_settings->settings->contains(group + "/" + key))
      {
         result.ok = false;
         result.info.append("KEY: <" + key + "> does not exists");
         return;
      }

      const QString path = system_settings->settings->value(key).toString();
      if (path != "")
      {
         if (!QFile::exists(path))
         {
            result.ok = false;
            result.info.append("FILE: <" + group + "/" + key + "> does not exists");
         }
      }

      //QLOG_DEBUG() << path;
   }};

   QStringList files_keys = {"darknet_path", "default_network", "default_weights"};
   system_settings->settings->beginGroup("FILES");
   for (auto key : files_keys) checkFile("FILES", key);
   system_settings->settings->endGroup();

   // [PATHS]
   auto checkPath{[system_settings, &result](const QString &group, const QString &key) {
      if (system_settings->settings->contains(group + "/" + key))
      {
         result.ok = false;
         result.info.append("KEY: <" + key + "> does not exists");
         return;
      }

      const QString path = system_settings->settings->value(key).toString();
      if (path == "default") return;

      if (path != "")
      {
         if (!QFile::exists(path))
         {
            result.ok = false;
            result.info.append("PATH: <" + group + "/" + key + "> does not exists");
         }
      }

      //QLOG_DEBUG() << path;
   }};

   QStringList paths_keys = {"default_backup_folder"};
   system_settings->settings->beginGroup("PATHS");
   for (auto key : paths_keys) checkPath("PATHS", key);
   system_settings->settings->endGroup();

   //QLOG_DEBUG() << "Total errors: " << result.info.size();
   if (!result.ok)
   {
      _ui->config_backB->setEnabled(false);
      QString error_message;
      for (auto info : result.info) error_message += info + "\n";

      //QLOG_DEBUG() << "Errors: " << result.info;

      QMessageBox::critical(this, tr("virtual-drone-box - system_config.ini"), error_message);
   }
   else
      _ui->config_backB->setEnabled(true);

   return result;
}
