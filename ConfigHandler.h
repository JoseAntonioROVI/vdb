#pragma once

//#include <QsLog/QsLog.h>


#include <QWidget>

#include <virtual_drone_box/SystemSettings.h>

#include "ui_MainWindow.h"

using namespace catec;

typedef struct
{
   bool ok;
   QStringList info;
} ConfigResult;

class ConfigHandler : public QWidget
{
   Q_OBJECT
  public:
   explicit ConfigHandler(QWidget *parent = nullptr, Ui::MainWindow *ui = nullptr);
   virtual ~ConfigHandler() {}

   void reset();

  private slots:
   void saveConfig();

  private:
   ConfigResult checkConfig();

   Ui::MainWindow *_ui;
};
