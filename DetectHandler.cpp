#include <QDir>
#include <QFileDialog>

#include <virtual_drone_box/SystemSettings.h>

#include "DetectHandler.h"

using namespace catec;

DetectHandler::DetectHandler(QWidget *parent, Ui::MainWindow *ui) : QWidget(parent)
{
   //QLOG_TRACE() << "DetectHandler::DetectHandler()";

    qRegisterMetaType<cv::Mat>("cv::Mat");

   _ui = ui;

   _browser_mapper = new QSignalMapper(this);
   _browser_mapper->setMapping(_ui->detector_imgsB, DetectHandler::INPUT_IMGS);
   //_browser_mapper->setMapping(_ui->detector_output_folderB, DetectHandler::OUTPUT_FOLDER);
   _browser_mapper->setMapping(_ui->detector_weightsB, DetectHandler::NETWORK_WEIGHTS);
   connect(_ui->detector_imgsB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   // connect(_ui->detector_output_folderB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_ui->detector_weightsB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_browser_mapper, SIGNAL(mapped(int)), this, SLOT(browseFolder(int)));

   connect(_ui->detector_startB, SIGNAL(clicked()), this, SLOT(start()));
   connect(_ui->bCamera, SIGNAL(clicked()), this, SLOT(clickedBCamera()));

   connect(this, &DetectHandler::updateSummary, this, &DetectHandler::setSummary);
   connect(&_detector, &DDetector::resultReady, this, &DetectHandler::processResults);

   QObject::connect(&_detector, SIGNAL(signalEndProcess()), this, SLOT(slotEndProcess()));

   reset();

   _isStarted = false;
}

void DetectHandler::reset()
{
   //QLOG_TRACE() << "DetectHandler::reset()";

   SystemSettings *system_settings = SystemSettings::getInstance();
   system_settings->settings->beginGroup("FILES");
   QString darknet_path = system_settings->settings->value("darknet_path").toString();
   system_settings->settings->endGroup();
   _detector.setPathToDarknet(darknet_path);

   _ui->detector_startB->setEnabled(false);
   _ui->detector_imgsTE->setText("");
   //_ui->detector_output_folderTE->setText("");
   _ui->detector_weightsTE->setText("");

   _detector.setInputFolder("");
   _detector.setOutputFolder("");
   _detector.setNetworkWeights("");
   _detector.setNetworkArch("");

   emit updateSummary(States::UNINITIALIZED);
}

void DetectHandler::browseFolder(int type)
{
   //QLOG_TRACE() << "DetectHandler::browseFolder()";
   _ui->detector_startB->setEnabled(false);

   QString directory;

   switch (type)
   {
      case BrowserType::INPUT_IMGS:
         directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(
             this, tr("Find folder")/*, QDir::currentPath(), QFileDialog::ShowDirsOnly*/));
         _ui->detector_imgsTE->setText(directory);

         if (_ui->bCamera->isChecked())
         {
             _ui->bCamera->setChecked(false);
             _ui->bCamera->setIcon(QIcon("resources/camara_icon.png"));
         }

         break;
         /*
               case BrowserType::OUTPUT_FOLDER:
                  directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(
                      this, tr("Find folder"), QDir::currentPath(), QFileDialog::ShowDirsOnly));
                  _ui->detector_output_folderTE->setText(directory);
                  break;
         */
      case BrowserType::NETWORK_WEIGHTS:
         directory =
             QDir::toNativeSeparators(QFileDialog::getOpenFileName(this, tr("Find weights file"), QDir::currentPath()));
         _ui->detector_weightsTE->setText(directory);
         break;
   }

   _detector.setNetworkWeights(_ui->detector_weightsTE->text());
   _detector.setInputFolder(_ui->detector_imgsTE->text());
   //_detector.setOutputFolder(_ui->detector_output_folderTE->text());

   SystemSettings *system_settings = SystemSettings::getInstance();

   system_settings->settings->beginGroup("FILES");
   QString network_arch_path = system_settings->settings->value("default_network").toString();
   system_settings->settings->endGroup();
   _detector.setNetworkArch(network_arch_path);

   system_settings->settings->beginGroup("FILES");
   QString darknet_path = system_settings->settings->value("darknet_path").toString();
   system_settings->settings->endGroup();
   _detector.setPathToDarknet(darknet_path);

   emit updateSummary(States::CONFIGURED);

   if (_detector.isReady() || (_ui->bCamera->isChecked() && !_ui->detector_weightsTE->text().isEmpty())) _ui->detector_startB->setEnabled(true);
}

void DetectHandler::start()
{

    if (!_isStarted)
    {
        emit updateSummary(States::DETECTING);
        //_ui->detector_startB->setEnabled(false);
        _ui->detector_startB->setText("Stop");
        _isStarted = true;
        if (!_ui->bCamera->isChecked())
        {
            //Arrancamos el thread para leer de fichero
            _detector.process();
        }
        else
        {
            //Arrancamos la captura de la cámara
            _detector.processWithCamera();
        }

    }
    else
    {
        _ui->detector_startB->setText("Start");
        _isStarted = false;
        if (!_ui->bCamera->isChecked())
        {
            //Paramos el thread
            _detector.stop();
        }
        else
        {
            _detector.stopWithCamera();
        }
    }

}

void DetectHandler::setSummary(int state)
{
   //QLOG_TRACE() << "DetectHandler::setSummary()";

   _ui->detector_summaryTB->clear();
   if (state == States::UNINITIALIZED)
   {
      _ui->detector_summaryTB->setText("No folders selected");
      return;
   }

   const int total_imgs = _detector.getImgsCount();
   _ui->detector_summaryTB->setText("Total Images: " + QString::number(total_imgs));

   if (state == States::CONFIGURED) return;
   _ui->detector_summaryTB->append("Detecting... ");
   if (state == States::DETECTING) return;
   _ui->detector_summaryTB->append("Done!");
}

void DetectHandler::processResults(QList<DetectionData> result_data)
{
   //QLOG_TRACE() << "DetectHandler::processResults()";
   emit updateSummary(States::DONE);
}

void DetectHandler::slotEndProcess()
{
    _ui->detector_startB->setText("Start");
    _isStarted = false;
}

void DetectHandler::clickedBCamera()
{
    if (!_ui->bCamera->isChecked())
    {
        _ui->bCamera->setIcon(QIcon("resources/camara_icon.png"));
        _ui->detector_startB->setEnabled(false);
    }
    else
    {
        _ui->bCamera->setIcon(QIcon("resources/camara_iconV.png"));
        _ui->detector_imgsTE->setText("");
        if (!_ui->detector_weightsTE->text().isEmpty())
        {
            _ui->detector_startB->setEnabled(true);
        }
    }
}
