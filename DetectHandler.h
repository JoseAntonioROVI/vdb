#pragma once

//#include <QsLog/QsLog.h>

#include <QSignalMapper>
#include <QWidget>

#include <virtual_drone_box/DDetector.h>

#include "ui_MainWindow.h"

using namespace catec;

class DetectHandler : public QWidget
{
   Q_OBJECT
  public:
   enum BrowserType
   {
      INPUT_IMGS,
      OUTPUT_FOLDER,
      NETWORK_WEIGHTS
   };

   explicit DetectHandler(QWidget *parent = nullptr, Ui::MainWindow *ui = nullptr);
   virtual ~DetectHandler() {}

   void reset();

  signals:
   void updateSummary(int state);

  private slots:
   void browseFolder(int type);
   void start();
   void setSummary(int state);
   void processResults(QList<DetectionData> result_data);
   //JSG_17_07_2020
   void slotEndProcess();
   //JSG_23_07_2020
   void clickedBCamera();
  private:
   enum States
   {
      UNINITIALIZED,
      CONFIGURED,
      DETECTING,
      DONE
   };

   Ui::MainWindow *_ui;
   QSignalMapper *_browser_mapper;

   DDetector _detector;

   //JSG_16_07_2020
   bool _isStarted;
};
