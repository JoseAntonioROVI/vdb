#include <QDir>
#include <QFileDialog>

#include <virtual_drone_box/SystemSettings.h>

#include "TrainHandler.h"

using namespace catec;

TrainHandler::TrainHandler(QWidget *parent, Ui::MainWindow *ui) : QWidget(parent)
{
   //QLOG_TRACE() << "TrainHandler::TrainHandler()";

   _ui = ui;

   _browser_mapper = new QSignalMapper(this);
   _browser_mapper->setMapping(_ui->train_imgsB, TrainHandler::TRAIN_IMGS);
   _browser_mapper->setMapping(_ui->valid_imgsB, TrainHandler::VALID_IMGS);
   _browser_mapper->setMapping(_ui->weights_backupB, TrainHandler::BACKUP_WEIGHTS);
   _browser_mapper->setMapping(_ui->initial_weightsB, TrainHandler::INITIAL_WEIGHTS);
   connect(_ui->train_imgsB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_ui->valid_imgsB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_ui->weights_backupB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_ui->initial_weightsB, SIGNAL(clicked()), _browser_mapper, SLOT(map()));
   connect(_browser_mapper, SIGNAL(mapped(int)), this, SLOT(browseFolder(int)));

   connect(_ui->train_startB, SIGNAL(clicked()), this, SLOT(start()));

   reset();
}

void TrainHandler::reset()
{
   //QLOG_TRACE() << "TrainHandler::reset()";

   SystemSettings *system_settings = SystemSettings::getInstance();
   system_settings->settings->beginGroup("FILES");
   _train.setPathToDarknet(system_settings->settings->value("darknet_path").toString());
   system_settings->settings->endGroup();

   _ui->train_startB->setEnabled(false);
   _ui->train_imgsTE->setText("");
   _ui->valid_imgsTB->setText("");
   _ui->weights_backupTB->setText("");
   _ui->initial_weightsTB->setText("Default");

   _train.setTrainImgs("");
   _train.setValidImgs("");
   _train.setBackupPath("");
   _train.setInitialWeights("");

   updateSummary(States::UNINITIALIZED);
}

void TrainHandler::browseFolder(int type)
{
   //QLOG_TRACE() << "TrainHandler::browseFolder()";
   _ui->train_startB->setEnabled(false);

   QString directory;

   switch (type)
   {
      case BrowserType::TRAIN_IMGS:
         directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(
             this, tr("Find folder"), QDir::currentPath(), QFileDialog::ShowDirsOnly));
         _ui->train_imgsTE->setText(directory);
         break;

      case BrowserType::VALID_IMGS:
         directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(
             this, tr("Find folder"), QDir::currentPath(), QFileDialog::ShowDirsOnly));
         _ui->valid_imgsTB->setText(directory);
         break;

      case BrowserType::BACKUP_WEIGHTS:
         directory = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(
             this, tr("Find folder"), QDir::currentPath(), QFileDialog::ShowDirsOnly));
         _ui->weights_backupTB->setText(directory);
         break;

      case BrowserType::INITIAL_WEIGHTS:
         directory = QDir::toNativeSeparators(QFileDialog::getOpenFileName(
             this, tr("Find weights file"), QDir::currentPath(), "weights file (*.weights)"));
         _ui->initial_weightsTB->setText(directory);
         break;
   }

   _train.setTrainImgs(_ui->train_imgsTE->text());
   _train.setValidImgs(_ui->valid_imgsTB->text());

   QString backup_path;
   if (_ui->weights_backupTB->text().toLower() == "default")
   {
      SystemSettings *system_settings = SystemSettings::getInstance();
      system_settings->settings->beginGroup("PATHS");
      backup_path = system_settings->settings->value("default_backup_folder").toString();
      system_settings->settings->endGroup();
   }
   else
      backup_path = _ui->weights_backupTB->text();
   _train.setBackupPath(backup_path);

   QString initial_weights_path;
   if (_ui->initial_weightsTB->text().toLower() == "default")
   {
      SystemSettings *system_settings = SystemSettings::getInstance();
      system_settings->settings->beginGroup("FILES");
      initial_weights_path = system_settings->settings->value("default_weights").toString();
      system_settings->settings->endGroup();
   }
   else
      initial_weights_path = _ui->initial_weightsTB->text();
   _train.setInitialWeights(initial_weights_path);

   SystemSettings *system_settings = SystemSettings::getInstance();
   system_settings->settings->beginGroup("FILES");
   QString network_arch_path = system_settings->settings->value("default_network").toString();
   system_settings->settings->endGroup();
   _train.setNetworkArch(network_arch_path);

   updateSummary(States::CONFIGURED);

   if (_train.isReady()) _ui->train_startB->setEnabled(true);
}

void TrainHandler::start()
{
   //QLOG_TRACE() << "TrainHandler::start()";

   updateSummary(States::TRAINING);
   _ui->train_startB->setEnabled(false);

   _train.train();
}

void TrainHandler::updateSummary(States state)
{
   //QLOG_TRACE() << "TrainHandler::updateSummary()";

   _ui->train_summaryTB->clear();
   if (state == States::UNINITIALIZED)
   {
      _ui->train_summaryTB->setText("No folders selected");
      return;
   }

   const int train_imgs = _train.getFilesNumber(Train::FilesType::TRAINING);
   const int valid_imgs = _train.getFilesNumber(Train::FilesType::VALIDATION);

   _ui->train_summaryTB->setText("Total Training Images: " + QString::number(train_imgs));
   _ui->train_summaryTB->append("Total Validation Images: " + QString::number(valid_imgs));
   if (state == States::CONFIGURED) return;
   _ui->train_summaryTB->append("Training... ");
}
