#pragma once

//#include <QsLog/QsLog.h>

#include <QSignalMapper>
#include <QWidget>

#include <virtual_drone_box/Train.h>

#include "ui_MainWindow.h"

using namespace catec;

class TrainHandler : public QWidget
{
   Q_OBJECT
  public:
   enum BrowserType
   {
      TRAIN_IMGS,
      VALID_IMGS,
      BACKUP_WEIGHTS,
      INITIAL_WEIGHTS
   };

   explicit TrainHandler(QWidget *parent = nullptr, Ui::MainWindow *ui = nullptr);
   virtual ~TrainHandler() {}

   void reset();

  private slots:
   void browseFolder(int type);
   void start();

  private:
   enum States
   {
      UNINITIALIZED,
      CONFIGURED,
      TRAINING
   };
   void updateSummary(States state);

   Ui::MainWindow *_ui;
   QSignalMapper *_browser_mapper;

   Train _train;
};
