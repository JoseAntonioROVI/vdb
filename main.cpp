#include <iostream>

#include <QApplication>
#include <QDateTime>
#include <QDir>

//#include <QsLog/QsLog.h>


#include <virtual_drone_box/config.h>

#include "mainwindow.h"


/// #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ##################
static void qtConfig();

int main(int argc, char *argv[])
{
   QApplication app(argc, argv);
   qtConfig();

   std::cout << "Starting up the application..." << std::endl;

   //configureLogger("virtual_drone_box_gui");

   MainWindow mainWin;
   mainWin.show();
   const int res = app.exec();

   std::cout << "Application shut down correctly" << std::endl;

   return res;
}

static void qtConfig()
{
   QCoreApplication::setOrganizationName("ROVIMATICA S.L");
   QCoreApplication::setApplicationName(QString("virtual_drone_box_gui-v%1").arg(PROJECT_VERSION));
}

