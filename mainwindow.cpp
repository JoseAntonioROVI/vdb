#include <QCloseEvent>
#include <QDebug>
#include <QSettings>
#include <QThread>

//#include <QsLog/QsLog.h>

#include <virtual_drone_box/SystemSettings.h>

#include "MainWindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), _ui(new Ui::MainWindow)
{
   //QLOG_TRACE() << "MainWindow::MainWindow()";

   _ui->setupUi(this);

   _ui->stacked_widget->setCurrentIndex(0);

   // Change page connections
   _buttons_signal_mapper = new QSignalMapper(this);
   _buttons_signal_mapper->setMapping(_ui->main_detectorB, 1);  // TODO: Index to Enum
   _buttons_signal_mapper->setMapping(_ui->main_trainB, 2);
   _buttons_signal_mapper->setMapping(_ui->main_configB, 3);
   _buttons_signal_mapper->setMapping(_ui->detect_backB, 0);
   _buttons_signal_mapper->setMapping(_ui->train_backB, 0);
   _buttons_signal_mapper->setMapping(_ui->config_backB, 0);
   connect(_ui->main_detectorB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_ui->main_trainB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_ui->main_configB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_ui->detect_backB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_ui->train_backB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_ui->config_backB, SIGNAL(clicked()), _buttons_signal_mapper, SLOT(map()));
   connect(_buttons_signal_mapper, SIGNAL(mapped(int)), this, SLOT(changePage(int)));

   //loadSettings();
   _ui->main_trainB->setVisible(false);


   _config_handler   = new ConfigHandler(this, _ui);
   _train_handler    = new TrainHandler(this, _ui);
   _detector_handler = new DetectHandler(this, _ui);
}

void MainWindow::changePage(const int &index)
{
//   QLOG_TRACE() << "MainWindow::changePage()";
   switch (index)
   {
      case 1:
         this->setWindowTitle("Detector - Virtual-Drone-Box");
         _detector_handler->reset();
         break;
      case 2:
         this->setWindowTitle("Train - Virtual-Drone-Box");
         _train_handler->reset();
         break;
      case 3:
         this->setWindowTitle("Configuration - Virtual-Drone-Box");
         _config_handler->reset();
         break;
      default:
         this->setWindowTitle("Virtual-Drone-Box");
         break;
   }

   _ui->stacked_widget->setCurrentIndex(index);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
   //QLOG_TRACE() << "MainWindow::closeEvent()";

   //saveSettings();
   event->accept();
}


//void MainWindow::saveSettings() { /*QLOG_TRACE() << "MainWindow::saveSettings()";*/ }

//void MainWindow::loadSettings() { /*QLOG_TRACE() << "MainWindow::loadSettings()";*/ }
