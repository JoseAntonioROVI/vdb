#pragma once

#include <memory>

#include <QMainWindow>
#include <QSignalMapper>

#include "ConfigHandler.h"
#include "DetectHandler.h"
#include "TrainHandler.h"


class MainWindow : public QMainWindow
{
   Q_OBJECT
  public:
   explicit MainWindow(QWidget *parent = nullptr);
   virtual ~MainWindow() {}

  private slots:
   void changePage(const int &index);

   void closeEvent(QCloseEvent *);


  private:
   //void saveSettings();
   //void loadSettings();

   ConfigHandler *_config_handler;
   TrainHandler *_train_handler;
   DetectHandler *_detector_handler;

   Ui::MainWindow *_ui;

   QSignalMapper *_buttons_signal_mapper;
};
