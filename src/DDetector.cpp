/*!
 *      @file  DDetector.cpp
 *    @author  Rafael Caballero González (RCG), rcaballero@catec.aero
 *
 *  @internal
 *    Created  28/4/2020
 *   Compiler  gcc/g++
 *    Company  FADA-CATEC
 *  Copyright (c) 2020, FADA-CATEC
 */

#include <iostream>

#include <virtual_drone_box/DetectionData.h>

#include <virtual_drone_box/DDetector.h>

namespace catec
{
DDetector::DDetector()
    : _darknet_dir(""), _network_weights_dir(""), _network_arch_dir(""), _input_folder_dir(""), _output_folder_dir("")
{
   qRegisterMetaType<QList<DetectionData>>("QList<DetectionData>");

   QDir tmp_folder("/tmp/virtual_drone_box/");
   if (!tmp_folder.exists() && !tmp_folder.mkpath(tmp_folder.absolutePath()))
   {
      std::cerr << "Error: Not tmp!!" << std::endl;
      return;
   }

   QDir tmp_train_folder("/tmp/virtual_drone_box/trainings");
   if (!tmp_train_folder.exists() && !tmp_train_folder.mkpath(tmp_train_folder.absolutePath()))
   {
      std::cerr << "Error: Not tmp!!" << std::endl;
      return;
   }
   //JSG_17_07_2020
   _filters << "*left*.png"
            << "*left*.jpg"
            << "*left*.jpeg"
            << "*left*.bmp";

   _detector = new DDetectorThread;
   _detector->moveToThread(&_detector_thread);
   connect(&_detector_thread, &QThread::finished, _detector, &QObject::deleteLater);
   connect(this, &DDetector::startProcessing, _detector, &DDetectorThread::process);
   // connect(_detector, &DDetectorThread::resultReady, this, &DDetector::handleResults);
   //JSG_16_07_2020
   connect(this, &DDetector::stopProcessing, _detector, &DDetectorThread::stop);
   connect(_detector, SIGNAL(signalSendEndProcess()), this, SLOT(slotEndProcess()));
   connect(_detector, SIGNAL(sendSignalMJPEGWriter(cv::Mat)), this, SLOT(slotReceiveMJPEGWriter(cv::Mat)));
   _detector_thread.start();

   _zed = new zedCamera();
   _zedThread = new QThread();
   _zed->moveToThread(_zedThread);
   QObject::connect(this,SIGNAL(signalInitializeCamera()),_zed,SLOT(initialize()));
   QObject::connect(this,SIGNAL(signalCloseCamera()),_zed,SLOT(closeConnection()));
   QObject::connect(_zed, SIGNAL(sendImageCV(cv::Mat)), _detector, SLOT(processSingleFrame(cv::Mat)));
   //Arrancamos el thread
   _zedThread->start();

   //Inicio del servidor
   _mjpeg.setPort(7777);
   _mjpeg.start();
}

DDetector::~DDetector()
{
   _detector_thread.quit();
   _detector_thread.wait();
   _mjpeg.stop(true);
}

void DDetector::setPathToDarknet(const QString &path)
{
   _darknet_path = path;
   _darknet_dir.setPath(_darknet_path);
}

void DDetector::setNetworkWeights(const QString &path)
{
   _network_weights_path = path;
   _network_weights_dir.setPath(_network_weights_path);
}

void DDetector::setNetworkArch(const QString &path)
{
   _network_arch_path = path;
   _network_arch_dir.setPath(_network_arch_path);
}

void DDetector::setInputFolder(const QString &path)
{
   if (path.isEmpty()) return;

   QString tmp_path = path;
   _input_folder_dir.setPath(tmp_path);
   _input_folder_dir.setNameFilters(_filters);

   if (path != _input_folder_path)
   {
      _input_folder_path = path;

      QFileInfoList imgs = _input_folder_dir.entryInfoList(_filters, QDir::Files | QDir::NoDotAndDotDot);

      _input_imgs_paths.clear();
      for (auto img : imgs) _input_imgs_paths.append(img.absoluteFilePath());
   }

   _input_folder_path = path;
}

void DDetector::setOutputFolder(const QString &path)
{
   if (path.isEmpty()) return;

   QString tmp_path = path;
   _output_folder_dir.setPath(tmp_path);

   if (path != _output_folder_path)
   {
      _output_folder_path = path;
   }

   _output_folder_path = path;
}

void DDetector::handleResults(QList<DetectionData> result_data)
{
   // TODO: save images
   QDir imgs_result_folder(_output_folder_path + "/imgs");
   if (!imgs_result_folder.exists() && !imgs_result_folder.mkpath(imgs_result_folder.absolutePath()))
   {
      std::cerr << "Error: Not imgs_result folder!!" << std::endl;
      return;
   }

   QDir txts_result_folder(_output_folder_path + "/data");
   if (!txts_result_folder.exists() && !txts_result_folder.mkpath(txts_result_folder.absolutePath()))
   {
      std::cerr << "Error: Not txts_result folder!!" << std::endl;
      return;
   }

   for (auto data : result_data)
   {
      QString img_path = data.img_file.absoluteFilePath();

      cv::Mat img = cv::imread(img_path.toStdString(), cv::IMREAD_COLOR);
      if (!img.data)
      {
         std::cout << "Image is empty! [" << img_path.toStdString() << "]" << std::endl;
         continue;
      }

      QString output_txt_path = txts_result_folder.absolutePath() + "/" + data.img_file.baseName() + ".txt";
      QFile detections_txt(output_txt_path);
      detections_txt.open(QIODevice::WriteOnly);

      for (auto bbox : data.detections)
      {
         cv::rectangle(img, cv::Point2i(bbox.x, bbox.y), cv::Point2i(bbox.x + bbox.w, bbox.y + bbox.h),
                       cv::Scalar(255, 0, 0), 2);

         // Format: id prob x y w h
         QString txt_line = QString::number(bbox.obj_id) + " " + QString::number(bbox.prob) + +" " +
                            QString::number(bbox.x) + " " + QString::number(bbox.y) + " " + QString::number(bbox.w) +
                            " " + QString::number(bbox.h);
         detections_txt.write(txt_line.toUtf8());
      }
      detections_txt.close();

      QString output_img_path = imgs_result_folder.absolutePath() + "/" + data.img_file.fileName();
      // cv::imwrite(output_img_path.toStdString(), img);
   }

   emit resultReady(result_data);
}

void DDetector::process()
{
    emit startProcessing(_network_arch_path, _network_weights_path, _input_imgs_paths);
}

void DDetector::processWithCamera()
{
    _detector->setNetworkPath(_network_arch_path, _network_weights_path);
    Q_EMIT signalInitializeCamera();
}

void DDetector::stopWithCamera()
{
    Q_EMIT signalCloseCamera();
    emit stopProcessing();
}

void DDetector::stop()
{
    emit stopProcessing();
}

int DDetector::getImgsCount() const { return _input_imgs_paths.size(); }

bool DDetector::isReady() const
{
   if (_darknet_path.isEmpty()) return false;

   if (_network_weights_path.isEmpty() || _network_arch_path.isEmpty() || _input_folder_path.isEmpty() /*||
       _output_folder_path.isEmpty()*/)
      return false;

   if (!_output_folder_dir.exists()) return false;

   return true;
}

void DDetector::slotEndProcess()
{
    Q_EMIT signalEndProcess();
}

void DDetector::slotReceiveMJPEGWriter(cv::Mat img)
{
    _mjpeg.write(img);
}

}  // namespace catec
