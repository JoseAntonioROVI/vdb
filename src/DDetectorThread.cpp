#include <virtual_drone_box/DDetectorThread.h>
#include <opencv2/core/core.hpp>
#include <opencv2/core/ocl.hpp>
//#include <opencv2/tracking.hpp>
#include <QDebug>

//#ifdef _WIN32
//#include <virtual_drone_box/MJPEGWriter.h>
//#endif


#define dt 0.04
namespace catec
{
DDetectorThread::DDetectorThread()
{
   SystemSettings *system_settings = SystemSettings::getInstance();
   system_settings->settings->beginGroup("PARAMETERS");
   host        = system_settings->settings->value("host").toString();
   port        = system_settings->settings->value("port").toInt();
   maxSize     = system_settings->settings->value("maxSize").toInt();
   maxDistance = system_settings->settings->value("maxDistance").toInt();
   maxFrames   = system_settings->settings->value("maxFrames").toInt();
   minConsFrames = system_settings->settings->value("minConsFrames").toInt();
   stepTime      = system_settings->settings->value("stepTime").toDouble();

   system_settings->settings->endGroup();


   _isStarted = false;

//#ifdef _WIN32
//   _mjpg.setPort(port);
//   _mjpg.start();
//   _isStarted = true;
//#endif

}
DDetectorThread::~DDetectorThread()
{
}


void DDetectorThread::process(const QString network_arch_path, const QString network_weights_path,
                              const QStringList inputs_paths)
{



   _detector = new Detector(network_arch_path.toStdString(), network_weights_path.toStdString());

   //QList<DetectionData> result_data;
//#ifdef _WIN32
//   MJPEGWriter mjpeg(port);
//   mjpeg.start();
//   _isStarted = true;
//#endif
   _isStarted = true;

   std::deque<tracker> tracks;
   for (auto input_path : inputs_paths)
   {
        if (_isStarted)
        {
            cv::Mat img = cv::imread(input_path.toStdString(), cv::IMREAD_COLOR);
            if (!img.data)
            {
               std::cout << "Image is empty! [" << input_path.toStdString() << "]" << std::endl;
               continue;
            }
            unsigned int img_width = img.size().width;
            unsigned int img_height = img.size().height;
            double maxDistance2 = (double)maxDistance/(double)img_width;

            std::vector<bbox_t> bboxes = _detector->detect(img, 0.4);


            //std::vector<bbox_t> bboxes = _detector->detect(img);
            //DetectionData img_result;
            //img_result.img_file = QFileInfo(input_path);

            std::deque<tracker> new_tracks;
            for (auto bboxT : bboxes)
            {
               // Track all detected bboxes
               new_tracks.emplace_back(bboxT, (double)maxFrames, maxDistance2, (double)0.04);
               new_tracks.back().ider((int)tracks.size() + (int)new_tracks.size());  // Give ID
               //cv::rectangle(img, cv::Rect(bboxT.x, bboxT.y, bboxT.w, bboxT.h), cv::Scalar(0, 255,0 )); //Green
               //qDebug() << bboxT.prob;
            }

            // COMPUTE DISTANCES FOR ASSIGNMENT
            unsigned int i = 0;
            std::vector<std::vector<double>> cost(tracks.size(), std::vector<double>(new_tracks.size()));
            for (auto track = tracks.begin(); track < tracks.end(); track++)
            {
                track->assigned = false;  // Assume old track wont be assigned to a new one
                track->min_cost = maxDistance2;
                // For each new detection: compute distance between existing tracks and new detections (newtrack)
                for (auto newtrack = new_tracks.begin(); newtrack != new_tracks.end(); newtrack++)
                {
                    std::vector<double> foo = track->predict();
                    double dist_norm2       = pow(((double)newtrack->cx - (double)foo[0]) / (double)img_width, 2) +
                            pow(((double)newtrack->cy - (double)foo[1]) / (double)img_height, 2);
                    double iou = track->iou(*newtrack);
                    cost[i].push_back(dist_norm2 / 2 - 0.1 * iou);  // note that min(dist)=min(dist^2)   : normalized (0,1)
                            if ((cost[i].back() < newtrack->min_cost) || newtrack->min_cost_track < 0)
                    {  // For each new track store his less cost old tracker
                        newtrack->min_dist       = dist_norm2;
                        newtrack->min_cost       = cost[i].back();
                        newtrack->min_cost_track = i;  // j;  // Less cost previous tracker for this new tracker is tracks[j]
                    }
                    if (newtrack->min_dist >= maxDistance2)  // newtrack->min_cost >= maxDistance2)
                    {                                        // si está muy lejos debe ser un nuevo drone
                        newtrack->min_cost_track = -1;
                    }
                }
                i++;
            }
            // ASIGNACION
            if (tracks.size() > 0)
            {  // If there was old tracks assigned
                for (auto newtrack = new_tracks.begin(); newtrack != new_tracks.end(); newtrack++)
                {
                    if (newtrack->min_cost_track < 0)
                    {  // if newtrack doesnt have previous track (cost >>)
                        newtrack->assigned = true;
                        newtrack->ider(tracks.back().id_number + 1);  // give it ID
                        newtrack->consecutiveFrames++;
                        tracks.push_back(*newtrack);  // add it
                    }
                    else
                    {  // cc newtrack has to have previous track
                        if (newtrack->min_cost < tracks[newtrack->min_cost_track].min_cost)
                        {  // if the assigned tracker cost its greater than this new track, assign the new
                            tracks[newtrack->min_cost_track].assignTrack(
                                        *newtrack);  // assign the coordinates of new tracks to theirs corresponding old tracks
                        }
                    }
                }
            }
            else
            {                        // if we had not tracks
                tracks = new_tracks;  // Track all new tracks
                for (auto track = tracks.begin(); track < tracks.end(); track++)
                {  // for(auto track:tracks){
                    track->assigned = true;
                    track->consecutiveFrames++;
                }
            }

#ifdef _dbg  // debug:
      std::cout << "newtracks" << std::endl;
      trackinfo(new_tracks);

      for (auto track : new_tracks)
      {
         cv::rectangle(img, cv::Point2i(track.tlx, track.tly), cv::Point2i(track.brx, track.bry), cv::Scalar(0, 255, 0),
                       2);  // Green
         cv::putText(img, track.id, cv::Point2i(track.brx, track.bry), cv::FONT_HERSHEY_SIMPLEX, 1.0,
                     cv::Scalar(0, 255, 0), 2);

         cv::circle(img, cv::Point(track.cx, track.cy), 5, cv::Scalar(0, 255, 0), -1);
      }  //
#endif

             // TRACKING
              new_tracks = tracks;
              tracks.clear();
              for (auto track = new_tracks.begin(); track != new_tracks.end(); track++)
              {
                  if (track->assigned == false)
                  {
                      track->consecutiveFrames = 0;
                      track->skippedFrames     = track->skippedFrames + 1;
                      if (track->skippedFrames > track->max_skippedFrames)
                      {  // Erase tracks we havent see in a long time
                          track->rmv = true;
                      }
                  }
                  else
                      track->skippedFrames = 0;  // rst skipped frames counter
                  if (track->consecutiveFrames > minConsFrames) track->false_positive = false;
                  if (!track->rmv)
                  {
                      track->doKalman();  // kalman filter applied to centroids
                      tracks.push_back(*track);
                  }
              }
              updateImages(img, tracks);

              cv::namedWindow("Window", cv::WindowFlags::WINDOW_NORMAL);
              cv::resizeWindow("Window", 1280, 720);
              cv::imshow("Window", img);

#ifdef _WIN32
            cv::waitKey(1);
            //mjpeg.write(img);
            Q_EMIT sendSignalMJPEGWriter(img);
#endif
            //result_data.append(img_result);

#ifdef _dbg  // debug:
      std::cout << "NEW OLD TRACKS" << std::endl;
      trackinfo(tracks);
      getchar();
      std::cout << "---------------------------------" << std::endl;
#endif
        }

   }

//#ifdef _WIN32
   //mjpeg.stop(true);
//#endif

   tracks.clear();
   cv::destroyWindow("Window");
   //Emitimos al interfaz para poner el boton de stop a start
   Q_EMIT signalSendEndProcess();
   //emit resultReady(result_data);
   _detector->~Detector();
   //vidi_free_buffer(&buffer);
   //vidi_deinitialize();
}

void DDetectorThread::processSingleFrame(cv::Mat img)
{
    if (_isStarted)
    {
        unsigned int img_width = img.size().width;
        unsigned int img_height = img.size().height;
        double maxDistance2 = (double)maxDistance/(double)img_width;

        std::vector<bbox_t> bboxes = _detector->detect(img, 0.4);


        //std::vector<bbox_t> bboxes = _detector->detect(img);
        //DetectionData img_result;
        //img_result.img_file = QFileInfo(input_path);

        std::deque<tracker> new_tracks;
        for (auto bboxT : bboxes)
        {
           // Track all detected bboxes
           new_tracks.emplace_back(bboxT, (double)maxFrames, maxDistance2, (double)0.04);
           new_tracks.back().ider((int)_tracks.size() + (int)new_tracks.size());  // Give ID
           //cv::rectangle(img, cv::Rect(bboxT.x, bboxT.y, bboxT.w, bboxT.h), cv::Scalar(0, 255,0 )); //Green
           //qDebug() << bboxT.prob;
        }

        // COMPUTE DISTANCES FOR ASSIGNMENT
        unsigned int i = 0;
        std::vector<std::vector<double>> cost(_tracks.size(), std::vector<double>(new_tracks.size()));
        for (auto track = _tracks.begin(); track < _tracks.end(); track++)
        {
            track->assigned = false;  // Assume old track wont be assigned to a new one
            track->min_cost = maxDistance2;
            // For each new detection: compute distance between existing tracks and new detections (newtrack)
            for (auto newtrack = new_tracks.begin(); newtrack != new_tracks.end(); newtrack++)
            {
                std::vector<double> foo = track->predict();
                double dist_norm2       = pow(((double)newtrack->cx - (double)foo[0]) / (double)img_width, 2) +
                        pow(((double)newtrack->cy - (double)foo[1]) / (double)img_height, 2);
                double iou = track->iou(*newtrack);
                cost[i].push_back(dist_norm2 / 2 - 0.1 * iou);  // note that min(dist)=min(dist^2)   : normalized (0,1)
                        if ((cost[i].back() < newtrack->min_cost) || newtrack->min_cost_track < 0)
                {  // For each new track store his less cost old tracker
                    newtrack->min_dist       = dist_norm2;
                    newtrack->min_cost       = cost[i].back();
                    newtrack->min_cost_track = i;  // j;  // Less cost previous tracker for this new tracker is tracks[j]
                }
                if (newtrack->min_dist >= maxDistance2)  // newtrack->min_cost >= maxDistance2)
                {                                        // si está muy lejos debe ser un nuevo drone
                    newtrack->min_cost_track = -1;
                }
            }
            i++;
        }
        // ASIGNACION
        if (_tracks.size() > 0)
        {  // If there was old tracks assigned
            for (auto newtrack = new_tracks.begin(); newtrack != new_tracks.end(); newtrack++)
            {
                if (newtrack->min_cost_track < 0)
                {  // if newtrack doesnt have previous track (cost >>)
                    newtrack->assigned = true;
                    newtrack->ider(_tracks.back().id_number + 1);  // give it ID
                    newtrack->consecutiveFrames++;
                    _tracks.push_back(*newtrack);  // add it
                }
                else
                {  // cc newtrack has to have previous track
                    if (newtrack->min_cost < _tracks[newtrack->min_cost_track].min_cost)
                    {  // if the assigned tracker cost its greater than this new track, assign the new
                        _tracks[newtrack->min_cost_track].assignTrack(
                                    *newtrack);  // assign the coordinates of new tracks to theirs corresponding old tracks
                    }
                }
            }
        }
        else
        {                        // if we had not tracks
            _tracks = new_tracks;  // Track all new tracks
            for (auto track = _tracks.begin(); track < _tracks.end(); track++)
            {  // for(auto track:tracks){
                track->assigned = true;
                track->consecutiveFrames++;
            }
        }

#ifdef _dbg  // debug:
  std::cout << "newtracks" << std::endl;
  trackinfo(new_tracks);

  for (auto track : new_tracks)
  {
     cv::rectangle(img, cv::Point2i(track.tlx, track.tly), cv::Point2i(track.brx, track.bry), cv::Scalar(0, 255, 0),
                   2);  // Green
     cv::putText(img, track.id, cv::Point2i(track.brx, track.bry), cv::FONT_HERSHEY_SIMPLEX, 1.0,
                 cv::Scalar(0, 255, 0), 2);

     cv::circle(img, cv::Point(track.cx, track.cy), 5, cv::Scalar(0, 255, 0), -1);
  }  //
#endif

         // TRACKING
          new_tracks = _tracks;
          _tracks.clear();
          for (auto track = new_tracks.begin(); track != new_tracks.end(); track++)
          {
              if (track->assigned == false)
              {
                  track->consecutiveFrames = 0;
                  track->skippedFrames     = track->skippedFrames + 1;
                  if (track->skippedFrames > track->max_skippedFrames)
                  {  // Erase tracks we havent see in a long time
                      track->rmv = true;
                  }
              }
              else
                  track->skippedFrames = 0;  // rst skipped frames counter
              if (track->consecutiveFrames > minConsFrames) track->false_positive = false;
              if (!track->rmv)
              {
                  track->doKalman();  // kalman filter applied to centroids
                  _tracks.push_back(*track);
              }
          }
          updateImages(img, _tracks);

          cv::namedWindow("Window", cv::WindowFlags::WINDOW_NORMAL);
          cv::resizeWindow("Window", 1280, 720);
          cv::imshow("Window", img);

            #ifdef _WIN32
                    cv::waitKey(1);
                    //mjpeg.write(img);
                    Q_EMIT sendSignalMJPEGWriter(img);
            #endif
                    //result_data.append(img_result);

            #ifdef _dbg  // debug:
              std::cout << "NEW OLD TRACKS" << std::endl;
              trackinfo(tracks);
              getchar();
              std::cout << "---------------------------------" << std::endl;
            #endif
    }
    else
    {
         cv::destroyAllWindows();
         _detector->~Detector();
    }
}

void DDetectorThread::stop()
{
    _isStarted = false;
}

void DDetectorThread::updateImages(cv::Mat &img, std::deque<tracker> tracks)
{
    for (auto track : tracks)
       {
          if (!track.false_positive)
          {
             if (track.assigned)
             {
                cv::rectangle(img, cv::Point2i(track.tlx, track.tly), cv::Point2i(track.brx, track.bry),
                              cv::Scalar(0, 0, 255), 2);  // BLUE
                cv::putText(img, track.id, cv::Point2i(track.brx, track.bry), cv::FONT_HERSHEY_SIMPLEX, 1.0,
                            cv::Scalar(0, 0, 255), 2);
             }
             else
             {
                cv::rectangle(img, cv::Point2i(track.tlx, track.tly), cv::Point2i(track.brx, track.bry),
                              cv::Scalar(0, 255, 0), 2);  // light BLUE
                cv::putText(img, track.id, cv::Point2i(track.brx, track.bry), cv::FONT_HERSHEY_SIMPLEX, 1.0,
                            cv::Scalar(0, 255, 0), 2);
             }
             for (unsigned int i = 1; i < track.tray.size(); i++)
             {
                cv::line(img, cv::Point2i(track.tray[i].x, track.tray[i].y),
                         cv::Point2i(track.tray[i - 1].x, track.tray[i - 1].y), cv::Scalar(193, 20, 255, 0.1), 2);
             }
             cv::circle(img, cv::Point(track.cx, track.cy), 2, cv::Scalar(0, 0, 255), -1);  // RED
          }
       }
}

void DDetectorThread::trackinfo(std::deque<tracker> tracks){
    for(auto track : tracks){
        std::cout << "+++  TRACK INFO +++" << std::endl;
        std::cout << "track  ";
        std::cout << track.id << std::endl;
        std::cout << "centroid    "<<track.cx << " " << track.cy << std::endl;
        std::cout << "assigned  " << track.assigned << std::endl;
        std::cout << "min_cost    " << track.min_cost << std::endl;
        std::cout << "min_cost_track    " << track.min_cost_track << std::endl;
        std::cout << "skipped frames    " << track.skippedFrames << std::endl;
        std::cout << "+++++++++++++++++++" << std::endl;
    }
}

void DDetectorThread::setNetworkPath(const QString network_arch_path, const QString network_weights_path)
{
    _detector = new Detector(network_arch_path.toStdString(), network_weights_path.toStdString());
    _isStarted = true;
    _tracks.clear();
}

}  // namespace catec
