#include "virtual_drone_box/MJPEGWriter.h"
#include <QTime>
#include <chrono>

//**********************************Public
MJPEGWriter::MJPEGWriter(int port)
    : quality(90)
    , port(port)
    ,_isStopped(false)

{
    _server = new QTcpServer (this);
}

void MJPEGWriter::setPort (int portLocal)
{
    port = portLocal;
}

MJPEGWriter::~MJPEGWriter()
{
    lastFrame.release();
}

void MJPEGWriter::write(cv::Mat frame)
{
    if (!frame.empty() && _listClients.size() > 0 && !_isStopped)
    {
        lastFrame.release();
        lastFrame = frame.clone();
        manageImagesToClients();
    }
}

void MJPEGWriter::start()
{
    startServer();
}

void MJPEGWriter::stop(bool isStopped)
{
    _isStopped = isStopped;
}


//Implementación de QT

void MJPEGWriter::startServer ()
{
    if (!_server->isListening())
    {
        QObject::connect (_server, SIGNAL(newConnection()), this, SLOT(newServerConnection()));
        _server->listen(QHostAddress::Any, port);
//        if (!_server->listen(QHostAddress::Any, port))
//        {
//            //qDebug() << "Server could not start";
//            Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Streaming server could not start");
//        }
//        else
//        {
//            //qDebug() << "Server started!";
//            Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Streaming server started!");
//        }
    }
}



void MJPEGWriter::newServerConnection()
{
    customSocket temp;
    temp.socket = _server->nextPendingConnection();
    QObject::connect (temp.socket, SIGNAL(readyRead()), this, SLOT (readyRead()));
    QObject::connect (temp.socket, SIGNAL (disconnected()), this, SLOT(disconnectedClient()));
    temp.readyToSendImages = false;
    _listClients.append(temp);
}

void MJPEGWriter::readyRead()
{
    QTcpSocket * socket = (QTcpSocket*)sender();
    QByteArray Data = socket->readAll();

    std::string header;
    header += "HTTP/1.0 200 OK\r\n";
    header += "Cache-Control: no-cache\r\n";
    header += "Pragma: no-cache\r\n";
    header += "Connection: close\r\n";
    header += "Content-Type: multipart/x-mixed-replace; boundary=mjpegstream\r\n\r\n";

    socket->write(header.c_str());

    for (int i = 0; i < _listClients.size(); ++i)
    {
        if (_listClients[i].socket->socketDescriptor() == socket->socketDescriptor())
        {
            _listClients[i].readyToSendImages = true;
            break;
        }
    }
    //qDebug() << "Cliente conectado " << socket->socketDescriptor();
    Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Connected client: " + QString::number(socket->socketDescriptor()));
}

void MJPEGWriter::disconnectedClient()
{
    QTcpSocket * socket = (QTcpSocket*)sender();
    for (int i = 0; i < _listClients.size(); ++i)
    {
        if (_listClients[i].socket->socketDescriptor() == socket->socketDescriptor())
        {
            _listClients.erase(_listClients.begin() + i);
            //qDebug() << "cliente desconectado";
            //Q_EMIT sendAddLogInfo (QTime::currentTime().toString("HH:mm:ss.zzz") + " Disconnected client");
            break;
        }
    }
}

void MJPEGWriter::manageImagesToClients()
{
    std::vector<uchar> outbuf;
    std::vector<int> params;
    params.push_back(cv::IMWRITE_JPEG_QUALITY);
    params.push_back(quality);
    if(!lastFrame.empty())
    {
        imencode(".jpg", lastFrame, outbuf, params);
        lastFrame.release();

        //QList<QFuture<void> > futures;
        for (int i = 0; i < _listClients.size(); ++i)
        {
            if (_listClients[i].readyToSendImages)
            {
                //Creamos el thread para enviar la imagen
                //auto future =  QtConcurrent::run(this, &MJPEGWriter::sendImageToClient, _listClients[i].socket, outbuf);
                //std::stringstream head;
                //head << "--mjpegstream\r\nContent-Type: image/jpeg\r\nContent-Length: " << outbuf.size() << "\r\n\r\n";
                //std::string string_head = head.str();
                std::string string_head = "--mjpegstream\r\nContent-Type: image/jpeg\r\nContent-Length: " + std::to_string(outbuf.size()) + "\r\n\r\n";
                _listClients[i].socket->write(string_head.c_str());
                _listClients[i].socket->write((char*)outbuf.data(), outbuf.size());
            }
        }
    }
}

//void MJPEGWriter::sendImageToClient(QTcpSocket * socket, std::vector<uchar> buffer)
//{
//    std::string string_head = "--mjpegstream\r\nContent-Type: image/jpeg\r\nContent-Length: " + std::to_string(buffer.size()) + "\r\n\r\n";
//    socket->write(string_head.c_str());
//    socket->write((char*)buffer.data());
//}


