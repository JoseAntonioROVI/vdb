/*!
 *      @file  SystemSettings.cpp
 *    @author  Rafael Caballero González (RCG), rcaballero@catec.aero
 *
 *  @internal
 *    Created  24/3/2020
 *   Compiler  gcc/g++
 *    Company  FADA-CATEC
 *  Copyright (c) 2020, FADA-CATEC
 */

#include <virtual_drone_box/SystemSettings.h>

namespace catec
{
SystemSettings* SystemSettings::getInstance()
{
   if (instance == nullptr) instance = new SystemSettings();
   return instance;
}

SystemSettings* SystemSettings::instance = 0;
}  // namespace catec