/*!
 *      @file  Train.cpp
 *    @author  Rafael Caballero González (RCG), rcaballero@catec.aero
 *
 *  @internal
 *    Created  23/3/2020
 *   Compiler  gcc/g++
 *    Company  FADA-CATEC
 *  Copyright (c) 2020, FADA-CATEC
 */

#include <iostream>

#include <virtual_drone_box/Train.h>

namespace catec
{
Train::Train() : _train_imgs_dir(""), _valid_imgs_dir(""), _backup_dir(""), _initial_weights_dir("")
{
   QDir tmp_folder("/tmp/virtual_drone_box/");
   if (!tmp_folder.exists() && !tmp_folder.mkpath(tmp_folder.absolutePath()))
   {
      std::cerr << "Error: Not tmp!!" << std::endl;
      return;
   }

   QDir tmp_train_folder("/tmp/virtual_drone_box/trainings");
   if (!tmp_train_folder.exists() && !tmp_train_folder.mkpath(tmp_train_folder.absolutePath()))
   {
      std::cerr << "Error: Not tmp!!" << std::endl;
      return;
   }

   _filters << "*.png"
            << "*.jpg"
            << "*.jpeg"
            << "*.bmp";
}

Train::~Train() {}

void Train::setPathToDarknet(const QString &path)
{
   _darknet_path = path;
   _darknet_dir.setPath(_darknet_path);
}

void Train::setTrainImgs(const QString &path)
{
   if (path.isEmpty()) return;

   QString tmp_path = path;
   _train_imgs_dir.setPath(tmp_path);
   _train_imgs_dir.setNameFilters(_filters);

   if (path != _train_imgs_path)
   {
      _train_imgs_path = path;
      createImgsTxtFile(FilesType::TRAINING);
   }

   _train_imgs_path = path;
}

void Train::setValidImgs(const QString &path)
{
   if (path.isEmpty()) return;

   QString tmp_path = path;
   _valid_imgs_dir.setPath(tmp_path);
   _valid_imgs_dir.setNameFilters(_filters);

   if (path != _valid_imgs_path)
   {
      _valid_imgs_path = path;
      createImgsTxtFile(FilesType::VALIDATION);
   }
   _valid_imgs_path = path;
}

void Train::setBackupPath(const QString &path)
{
   _backup_path = path;
   _backup_dir.setPath(_backup_path);
}

void Train::setInitialWeights(const QString &path)
{
   _initial_weights_path = path;
   _initial_weights_dir.setPath(_initial_weights_path);
}

void Train::setNetworkArch(const QString &path)
{
   _network_arch_path = path;
   _network_arch_dir.setPath(_network_arch_path);
}

void Train::train()
{
   createObjFiles();

   QString command = _darknet_path + " detector train " +
                     _darknet_dir.relativeFilePath("/tmp/virtual_drone_box/obj.data") + " " + _network_arch_path + " " +
                     _initial_weights_path + " -map; exec bash";

   std::cout << command.toStdString() << std::endl;

   std::string cmd = "gnome-terminal -- /bin/sh -c '" + command.toStdString() + "'";
   system(cmd.c_str());
}

void Train::stop() {}

bool Train::isReady() const
{
   if (_darknet_path.isEmpty()) return false;

   if (_train_imgs_path.isEmpty() || _valid_imgs_path.isEmpty() || _backup_path.isEmpty()) return false;

   if (!_train_imgs_dir.exists() && !_valid_imgs_dir.exists() && !_backup_dir.exists()) return false;

   if (_train_imgs_count <= 0 || _valid_imgs_count <= 0) return false;

   return true;
}

int Train::getFilesNumber(FilesType type) const
{
   if (type == Train::FilesType::TRAINING)
      return _train_imgs_count;
   else if (type == Train::FilesType::VALIDATION)
      return _valid_imgs_count;
   else
      return 0;
}

void Train::createImgsTxtFile(FilesType type)
{
   QDir tmp_folder(QDir::home().absoluteFilePath(QString("/tmp/virtual_drone_box/")));

   auto saveImages{[this, type](QDir *imgs_folder, QFile *file) {
      QFileInfoList imgs = imgs_folder->entryInfoList(_filters, QDir::Files | QDir::NoDotAndDotDot);

      file->open(QIODevice::WriteOnly);
      for (auto img : imgs)
      {
#ifdef NDEBUG
         QString txt_file_path = imgs_folder->path() + "/" + img.baseName() + ".txt";
         if (!QFile(txt_file_path).exists()) continue;
#endif
         file->write(("/" + _darknet_dir.relativeFilePath(img.absoluteFilePath()) + "\n").toUtf8());

         if (type == FilesType::TRAINING) _train_imgs_count++;
         if (type == FilesType::VALIDATION) _valid_imgs_count++;
      }
      file->close();
   }};

   QFile *file;
   if (type == FilesType::TRAINING)
   {
      file = new QFile(tmp_folder.path() + QString("/train_imgs.txt"));

      _train_imgs_count = 0;
      saveImages(&_train_imgs_dir, file);
   }
   else if (type == FilesType::VALIDATION)
   {
      file = new QFile(tmp_folder.path() + QString("/valid_imgs.txt"));

      _valid_imgs_count = 0;
      saveImages(&_valid_imgs_dir, file);
   }
}

void Train::createObjFiles()
{
   // TODO: try catch?

   QFile obj_names_file("/tmp/virtual_drone_box/obj.names");
   obj_names_file.open(QIODevice::WriteOnly);
   obj_names_file.write("drone\n");
   obj_names_file.close();

   QFile obj_data_file("/tmp/virtual_drone_box/obj.data");
   obj_data_file.open(QIODevice::WriteOnly);
   obj_data_file.write("classes=1\n");

   QString rel_to_train_txt = _darknet_dir.relativeFilePath("/tmp/virtual_drone_box/train_imgs.txt");
   obj_data_file.write("train=" + rel_to_train_txt.toUtf8() + "\n");

   QString rel_to_valid_txt = _darknet_dir.relativeFilePath("/tmp/virtual_drone_box/valid_imgs.txt");
   obj_data_file.write("valid=" + rel_to_valid_txt.toUtf8() + "\n");

   QString rel_to_names_txt = _darknet_dir.relativeFilePath("/tmp/virtual_drone_box/obj.names");
   obj_data_file.write("names=" + rel_to_names_txt.toUtf8() + "\n");

   QString rel_to_backup_folder = _darknet_dir.relativeFilePath(_backup_path);
   obj_data_file.write("backup=" + rel_to_backup_folder.toUtf8() + "/\n");

   obj_data_file.close();
}

}  // namespace catec