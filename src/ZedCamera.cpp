#include <virtual_drone_box/ZedCamera.h>


zedCamera::zedCamera(QObject *parent) : QObject(parent)
{

}

zedCamera::~zedCamera()
{

}

void zedCamera::initialize()
{
    //Config camera parameters
    _initParametersZed.sdk_verbose = false; //Disable verbose mode
    _initParametersZed.camera_resolution = sl::RESOLUTION::HD2K; //HD2k@2208*1242, HD1080@1920*1080, HD720@1280*720, VGA@672*376
    _initParametersZed.camera_fps = 15; //HD2k@15fps, HD1080@15<->30fps, HD720@15<->30<->60fps, VGA@15<->30<->60<->100fps, If set to 0, the highest FPS of the specified
    _initParametersZed.coordinate_units = sl::UNIT::METER;
    _initParametersZed.depth_mode = sl::DEPTH_MODE::ULTRA;
    _initParametersZed.depth_stabilization = true;
    _initParametersZed.coordinate_system = sl::COORDINATE_SYSTEM::RIGHT_HANDED_Y_UP;
    _initParametersZed.camera_disable_self_calib = true;
    _initParametersZed.depth_maximum_distance = 40;

    // TO DO cambiar estos valores desde interfaz o fichero de configuracion
    _initParametersZed.depth_minimum_distance = 3; //maximo segun la API https://www.stereolabs.com/docs/api/structsl_1_1InitParameters.html#a115c93ba994ec7b5c853375d647a936e
    //_zedCam.setDepthMaxRangeValue(25);

    //Open camera
    sl::ERROR_CODE err = _zedCam.open(_initParametersZed);
    if(err != sl::ERROR_CODE::SUCCESS)
    {
        //Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Camera not found, error: " + QString(sl::toString(err)));
        //Q_EMIT connectionFAIL();
        //exit(-1);
    }
    else
    {
        //Get camera info
        uint zedCamSerial = _zedCam.getCameraInformation().serial_number;
        //Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Zed camera connected with serial number: " + QString::number(zedCamSerial));
    }

    //_flagFinish = false;

    //Queue for save images to disk
    //_queueImLeft = new QQueue<sl::Mat>();
    //_queueImRight = new QQueue<sl::Mat>();
    //_queueImDepth = new QQueue<sl::Mat>();
    //_queueStrImLeft = new QQueue<QString>();
    //_queueStrImRight = new QQueue<QString>();
    //_queueStrImDepth = new QQueue<QString>();
    _timerGrabImage = new QTimer();
    _timerGrabImage->setInterval(1000*(1/FPS_ZED)); // 15fps --> (1/15)*1000 ms => 66.66666666667ms
    _timerGrabImage->setTimerType(Qt::PreciseTimer);
    QObject::connect(_timerGrabImage,SIGNAL(timeout()),this,SLOT(capture()));
    _timerGrabImage->start();
}


void zedCamera::closeConnection()
{
    //_depthMeasure.free();
    _timerGrabImage->stop();
    //_zedCam.close();
}

/*!
 * \brief zedCamera::capture
 * Method to grab an image
 */
void zedCamera::capture()
{
    //_flagFinish = false;

    sl::RuntimeParameters runtime_param;
    runtime_param.sensing_mode = sl::SENSING_MODE::STANDARD;

    sl::Resolution slImSize = _zedCam.getCameraInformation().camera_resolution; //HD2k@2208*1242

    sl::Mat imageLeft(slImSize.width, slImSize.height, sl::MAT_TYPE::U8_C4);
    cv::Mat imageLeftCV = slMat2cvMat(imageLeft); //Como ya tienen la memoria compartida cualquier operacion sobre sl::Mat estara reflejada en cv::Mat
    sl::Mat imageRight(slImSize.width, slImSize.height, sl::MAT_TYPE::U8_C4);
    cv::Mat imageRightCV = slMat2cvMat(imageRight);
    sl::Mat imageDepth(slImSize.width, slImSize.height, sl::MAT_TYPE::F32_C1);
    cv::Mat imageDepthCV = slMat2cvMat(imageDepth);
    sl::Mat imageDepthXYZ(slImSize.width,slImSize.height, sl::MAT_TYPE::F32_C4);
    cv::Mat imageDepthXYZCV = slMat2cvMat(imageDepthXYZ);

    //QImage qLeft, qRight, qDepth;
    //Q_EMIT sendAddLogInfo(QTime::currentTime().toString("HH:mm:ss.zzz") + " Image acquisition initialized");

    //while(!_flagFinish)
    //{
    if(_zedCam.grab(runtime_param) == sl::ERROR_CODE::SUCCESS /*&& !_flagFinish*/)
    {
        //QString timestamp = QTime::currentTime().toString("HH_mm_ss_zzz");
        _zedCam.retrieveImage(imageLeft,sl::VIEW::LEFT);
        _zedCam.retrieveImage(imageRight,sl::VIEW::RIGHT);
        _zedCam.retrieveMeasure(imageDepth,sl::MEASURE::DEPTH);
        _zedCam.retrieveMeasure(imageDepthXYZ,sl::MEASURE::XYZ);

        Q_EMIT sendImageCV(imageLeftCV.clone());
    }

}

cv::Mat zedCamera::slMat2cvMat(sl::Mat& input)
{
    // Mapping between MAT_TYPE and CV_TYPE
    int cv_type = -1;
    switch (input.getDataType()) {
        case sl::MAT_TYPE::F32_C1: cv_type = CV_32FC1; break;
        case sl::MAT_TYPE::F32_C2: cv_type = CV_32FC2; break;
        case sl::MAT_TYPE::F32_C3: cv_type = CV_32FC3; break;
        case sl::MAT_TYPE::F32_C4: cv_type = CV_32FC4; break;
        case sl::MAT_TYPE::U8_C1: cv_type = CV_8UC1; break;
        case sl::MAT_TYPE::U8_C2: cv_type = CV_8UC2; break;
        case sl::MAT_TYPE::U8_C3: cv_type = CV_8UC3; break;
        case sl::MAT_TYPE::U8_C4: cv_type = CV_8UC4; break;
        default: break;
    }
    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    return cv::Mat(input.getHeight(), input.getWidth(), cv_type, input.getPtr<sl::uchar1>(sl::MEM::CPU));
}
