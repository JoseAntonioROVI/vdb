/*
    Kalman filter, Miguel López, 05/2020
    rlopez.ext@catec.aereo
*/

#include "virtual_drone_box/kalman.h"
#include <iostream>

#define nvar 6  // number of state's var  x,y,vx,vy
#define nmes 6  // number of measurements variables x,y,vx,vy

kalman::kalman(double time_step)
    : dt(time_step),
      xk1(nvar, 1),
      xk(nvar, 1),
      z(nvar, 1),
      F(nvar, nvar),
      P(nvar, nvar),
      Q(nvar, nvar),
      R(nmes, nmes),
      H(nmes, nvar)
{  // R,2,2 h2,4
   ini = false;
   F << 1, 0, dt, 0, 0, 0, 0, 1, 0, dt, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
       1;  // state-transition model
   P << 1000, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 0,
       0, 0, 0, 1000;  // initial estimated cov
   Q << .05, 0, .05, 0, 0, 0, 0, .05, 0, .05, 0, 0, 0, 0, .05, 0, 0, 0, 0, 0, 0, .05, 0, 0, 0, 0, 0, 0, .05, 0, 0, 0, 0,
       0, 0, .05;  // process cov (external noise)
   R << .01, 0, 0, 0, 0, 0, 0, .01, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, .01, 0, 0, 0, 0, 0, 0,
       .01;  // observation cov (sensor noise)
   H << 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
       1;  // observation model
}

void kalman::predict()
{
   if (ini == false)
   {
      vx = 0;
      vy = 0;
      xk1 << input_x, input_y, vx, vy, w,
          h;  // Initial stimated state xk1=[x0, y0, 0, 0] (being state x=[x, y, vx, vy])
      ini = true;
   }
   else
   {
      vx = (input_x - filtered_x) / dt;
      vy = (input_y - filtered_y) / dt;
   }

   xk = F * xk1;
   P  = F * P * F.transpose() + Q;
}

void kalman::update(bool isdata)
{
   z << input_x, input_y, vx, vy, w, h;  // measurement position and speed

   if (isdata)
   {  // if there is data: update
      K   = P * H.transpose() * (H * P * H.transpose() + R).inverse();
      xk1 = xk + K * (z - H * xk);
      P   = P - K * H * P;
   }
   else
      xk1 = xk;  // if not: next estimate is just the predict

   filtered_x = xk1(0);
   filtered_y = xk1(1);
   filtered_w = xk1(4);
   filtered_h = xk1(5);
}

void kalman::iterate(double x, double y, double width, double height, bool isdata)
{  // isdata=false if there is no data
   if (isdata)
   {
      input_x = x;
      input_y = y;
      w       = width;
      h       = height;
   }
   predict();
   update(isdata);
}
