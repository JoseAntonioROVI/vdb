/*
    Tracker for vdb, Miguel López, 05/2020
    rlopez.ext@catec.aereo
*/

//#include "kalman.h"
//#include <virtual_drone_box/darknet/yolo_v2_class.hpp>
#include "virtual_drone_box/tracker.h"

tracker::tracker(bbox_t bbox, unsigned int max_skippedFrames, double squared_max_Distance, double time_step)
    : tlx(bbox.x),
      tly(bbox.y),
      w(bbox.w),
      h(bbox.h),
      max_skippedFrames(max_skippedFrames),
      min_cost(squared_max_Distance),
      min_dist(squared_max_Distance),
      time_step(time_step)
{
   cx                = tlx + w / 2;
   cy                = tly + h / 2;
   brx               = tlx + w;
   bry               = tly + h;
   assigned          = false;
   false_positive    = true;
   consecutiveFrames = 0;
   min_cost_track    = -1;
   skippedFrames     = 0;
   ussingKalman      = false;
   rmv               = false;
}

void tracker::assignTrack(tracker trackToAssign)
{  // copy data from trackToAssign to this tracker and mark it as assigned
   cx  = trackToAssign.cx;
   cy  = trackToAssign.cy;
   tlx = trackToAssign.tlx;
   tly = trackToAssign.tly;
   brx = trackToAssign.brx;
   bry = trackToAssign.bry;

   w = trackToAssign.w;
   h = trackToAssign.h;

   vx = trackToAssign.vx;
   vy = trackToAssign.vy;

   min_cost = trackToAssign.min_cost;
   consecutiveFrames++;
   assigned = true;
}

void tracker::addtray(double x, double y)
{
   coord c;
   c.x = cx;
   c.y = cy;
   if ((tray.size() >= tray.max_size()) || (tray.size() >= 200))
   {
      tray.pop_front();
   }
   tray.push_back(c);
}

void tracker::doKalman()
{
   if (!ussingKalman)
   {  // if not kalman filer assigned to this track, create it
      usedKalman   = kalman((double)time_step);
      ussingKalman = true;
   }
   usedKalman.iterate(cx, cy, w, h, assigned);
   cx = usedKalman.filtered_x;
   cy = usedKalman.filtered_y;
   w  = usedKalman.filtered_w;
   h  = usedKalman.filtered_h;

   tlx = cx - w / 2;
   tly = cy - h / 2;
   brx = cx + w / 2;
   bry = cy + h / 2;

   vx    = usedKalman.vx;
   vy    = usedKalman.vy;
   vmod2 = sqrt(pow(vx, 2) + pow(vy, 2));

   addtray(cx, cy);
}

void tracker::ider(int nid)
{
   id_number = nid;
   id        = "Drone" + std::to_string(nid);
}

std::vector<double> tracker::predict()
{
   if (!ussingKalman)
   {  // if not kalman filer assigned to this track, create it
      usedKalman   = kalman();
      ussingKalman = true;
   }
   usedKalman.input_x = cx;
   usedKalman.input_x = cy;
   usedKalman.predict();

   tlx = usedKalman.xk(0) - w / 2;
   tly = usedKalman.xk(1) - h / 2;
   brx = usedKalman.xk(0) + w / 2;
   bry = usedKalman.xk(1) + h / 2;

   std::vector<double> foo = {usedKalman.xk(0),
                              usedKalman.xk(1)};  //, usedKalman.xk(0)-w/2, usedKalman.xk(1)-h/2, usedKalman.xk(0)+w/2
                                                  //,usedKalman.xk(1)+h/2};//cx,cy,tlx,tly,brx,bry
   return foo;
}

double tracker::iou(tracker trackToCompare)
{  // Intersection over union
   double xA = std::max(tlx, trackToCompare.tlx);
   double yA = std::max(tly, trackToCompare.tly);
   double xB = std::min(brx, trackToCompare.brx);
   double yB = std::min(bry, trackToCompare.bry);

   double inter = std::max(0.0, xB - xA + 1) * std::max(0.0, yB - yA + 1);

   double thisArea    = (brx - tlx + 1) * (bry - tly + 1);
   double compareArea = (trackToCompare.brx - trackToCompare.tlx + 1) * (trackToCompare.bry - trackToCompare.tly + 1);

   double iou = inter / (double)(thisArea + compareArea - inter);

   return iou;
}
