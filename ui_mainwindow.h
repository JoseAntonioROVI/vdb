/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *central_widget;
    QGridLayout *gridLayout_2;
    QStackedWidget *stacked_widget;
    QWidget *main_menu_page;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *main_menu_leftSP;
    QGroupBox *main_pageGB;
    QVBoxLayout *verticalLayout_2;
    QPushButton *main_detectorB;
    QPushButton *main_trainB;
    QPushButton *main_configB;
    QSpacerItem *main_menu_rightSP;
    QWidget *detector_page;
    QGridLayout *gridLayout;
    QWidget *widget_14;
    QHBoxLayout *horizontalLayout_27;
    QSpacerItem *horizontalSpacer_10;
    QFrame *frame_5;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_17;
    QPushButton *bCamera;
    QLabel *label_9;
    QLineEdit *detector_imgsTE;
    QPushButton *detector_imgsB;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer_16;
    QLabel *label_11;
    QLineEdit *detector_weightsTE;
    QPushButton *detector_weightsB;
    QSpacerItem *horizontalSpacer_11;
    QWidget *widget_26;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_14;
    QTextBrowser *detector_summaryTB;
    QSpacerItem *horizontalSpacer_15;
    QWidget *widget_25;
    QHBoxLayout *horizontalLayout_26;
    QSpacerItem *horizontalSpacer_13;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_25;
    QPushButton *detector_startB;
    QSpacerItem *horizontalSpacer_12;
    QWidget *widget_13;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *detect_backB;
    QSpacerItem *detector_page_rightSP;
    QWidget *train_page;
    QVBoxLayout *verticalLayout_3;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_7;
    QWidget *widget_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_4;
    QWidget *widget_5;
    QVBoxLayout *verticalLayout_5;
    QWidget *widget_7;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *train_imgsTE;
    QPushButton *train_imgsB;
    QWidget *widget_10;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *valid_imgsTB;
    QPushButton *valid_imgsB;
    QWidget *widget_9;
    QHBoxLayout *horizontalLayout_8;
    QLineEdit *weights_backupTB;
    QPushButton *weights_backupB;
    QWidget *widget_8;
    QHBoxLayout *horizontalLayout_6;
    QLineEdit *initial_weightsTB;
    QPushButton *initial_weightsB;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_7;
    QTextBrowser *train_summaryTB;
    QSpacerItem *horizontalSpacer_8;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_12;
    QPushButton *train_startB;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *train_backB;
    QSpacerItem *horizontalSpacer_3;
    QWidget *config_page;
    QVBoxLayout *verticalLayout;
    QWidget *widget_12;
    QHBoxLayout *horizontalLayout_2;
    QPlainTextEdit *configPTE;
    QWidget *widget_11;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *config_backB;
    QSpacerItem *config_page_rightSP;
    QPushButton *config_saveB;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_9;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(678, 600);
        MainWindow->setMaximumSize(QSize(800, 600));
        central_widget = new QWidget(MainWindow);
        central_widget->setObjectName(QStringLiteral("central_widget"));
        central_widget->setEnabled(true);
        gridLayout_2 = new QGridLayout(central_widget);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        stacked_widget = new QStackedWidget(central_widget);
        stacked_widget->setObjectName(QStringLiteral("stacked_widget"));
        stacked_widget->setMaximumSize(QSize(733, 630));
        main_menu_page = new QWidget();
        main_menu_page->setObjectName(QStringLiteral("main_menu_page"));
        horizontalLayout = new QHBoxLayout(main_menu_page);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        main_menu_leftSP = new QSpacerItem(245, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(main_menu_leftSP);

        main_pageGB = new QGroupBox(main_menu_page);
        main_pageGB->setObjectName(QStringLiteral("main_pageGB"));
        main_pageGB->setMaximumSize(QSize(300, 400));
        main_pageGB->setAlignment(Qt::AlignCenter);
        verticalLayout_2 = new QVBoxLayout(main_pageGB);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        main_detectorB = new QPushButton(main_pageGB);
        main_detectorB->setObjectName(QStringLiteral("main_detectorB"));
        main_detectorB->setMinimumSize(QSize(160, 60));

        verticalLayout_2->addWidget(main_detectorB);

        main_trainB = new QPushButton(main_pageGB);
        main_trainB->setObjectName(QStringLiteral("main_trainB"));
        main_trainB->setMinimumSize(QSize(160, 60));

        verticalLayout_2->addWidget(main_trainB);

        main_configB = new QPushButton(main_pageGB);
        main_configB->setObjectName(QStringLiteral("main_configB"));
        main_configB->setMinimumSize(QSize(160, 60));

        verticalLayout_2->addWidget(main_configB);


        horizontalLayout->addWidget(main_pageGB);

        main_menu_rightSP = new QSpacerItem(244, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(main_menu_rightSP);

        stacked_widget->addWidget(main_menu_page);
        detector_page = new QWidget();
        detector_page->setObjectName(QStringLiteral("detector_page"));
        gridLayout = new QGridLayout(detector_page);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        widget_14 = new QWidget(detector_page);
        widget_14->setObjectName(QStringLiteral("widget_14"));
        horizontalLayout_27 = new QHBoxLayout(widget_14);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_27->addItem(horizontalSpacer_10);

        frame_5 = new QFrame(widget_14);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        frame_5->setMinimumSize(QSize(450, 0));
        frame_5->setFrameShape(QFrame::StyledPanel);
        gridLayout_3 = new QGridLayout(frame_5);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        bCamera = new QPushButton(frame_5);
        bCamera->setObjectName(QStringLiteral("bCamera"));
        QIcon icon;
        icon.addFile(QStringLiteral("resources/camara_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        bCamera->setIcon(icon);
        bCamera->setIconSize(QSize(30, 30));
        bCamera->setCheckable(true);

        horizontalLayout_17->addWidget(bCamera);

        label_9 = new QLabel(frame_5);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        horizontalLayout_17->addWidget(label_9);

        detector_imgsTE = new QLineEdit(frame_5);
        detector_imgsTE->setObjectName(QStringLiteral("detector_imgsTE"));
        detector_imgsTE->setMinimumSize(QSize(30, 30));
        detector_imgsTE->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_17->addWidget(detector_imgsTE);

        detector_imgsB = new QPushButton(frame_5);
        detector_imgsB->setObjectName(QStringLiteral("detector_imgsB"));
        detector_imgsB->setMinimumSize(QSize(30, 30));
        detector_imgsB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_17->addWidget(detector_imgsB);


        verticalLayout_6->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        horizontalSpacer_16 = new QSpacerItem(75, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_16);

        label_11 = new QLabel(frame_5);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(label_11);

        detector_weightsTE = new QLineEdit(frame_5);
        detector_weightsTE->setObjectName(QStringLiteral("detector_weightsTE"));
        detector_weightsTE->setMinimumSize(QSize(30, 30));
        detector_weightsTE->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_18->addWidget(detector_weightsTE);

        detector_weightsB = new QPushButton(frame_5);
        detector_weightsB->setObjectName(QStringLiteral("detector_weightsB"));
        detector_weightsB->setMinimumSize(QSize(30, 30));
        detector_weightsB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_18->addWidget(detector_weightsB);


        verticalLayout_6->addLayout(horizontalLayout_18);


        gridLayout_3->addLayout(verticalLayout_6, 0, 0, 1, 1);


        horizontalLayout_27->addWidget(frame_5);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_27->addItem(horizontalSpacer_11);


        gridLayout->addWidget(widget_14, 0, 0, 1, 1);

        widget_26 = new QWidget(detector_page);
        widget_26->setObjectName(QStringLiteral("widget_26"));
        horizontalLayout_15 = new QHBoxLayout(widget_26);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalSpacer_14 = new QSpacerItem(143, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_14);

        detector_summaryTB = new QTextBrowser(widget_26);
        detector_summaryTB->setObjectName(QStringLiteral("detector_summaryTB"));
        detector_summaryTB->setMinimumSize(QSize(320, 0));

        horizontalLayout_15->addWidget(detector_summaryTB);

        horizontalSpacer_15 = new QSpacerItem(143, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_15);


        gridLayout->addWidget(widget_26, 1, 0, 1, 1);

        widget_25 = new QWidget(detector_page);
        widget_25->setObjectName(QStringLiteral("widget_25"));
        horizontalLayout_26 = new QHBoxLayout(widget_25);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        horizontalSpacer_13 = new QSpacerItem(233, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_26->addItem(horizontalSpacer_13);

        frame_2 = new QFrame(widget_25);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_25 = new QHBoxLayout(frame_2);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        detector_startB = new QPushButton(frame_2);
        detector_startB->setObjectName(QStringLiteral("detector_startB"));
        detector_startB->setMinimumSize(QSize(120, 40));
        detector_startB->setMaximumSize(QSize(140, 60));

        horizontalLayout_25->addWidget(detector_startB);


        horizontalLayout_26->addWidget(frame_2);

        horizontalSpacer_12 = new QSpacerItem(233, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_26->addItem(horizontalSpacer_12);


        gridLayout->addWidget(widget_25, 2, 0, 1, 1);

        widget_13 = new QWidget(detector_page);
        widget_13->setObjectName(QStringLiteral("widget_13"));
        widget_13->setMinimumSize(QSize(0, 40));
        widget_13->setMaximumSize(QSize(16777215, 40));
        horizontalLayout_4 = new QHBoxLayout(widget_13);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        detect_backB = new QPushButton(widget_13);
        detect_backB->setObjectName(QStringLiteral("detect_backB"));
        detect_backB->setEnabled(true);
        detect_backB->setMinimumSize(QSize(0, 0));

        horizontalLayout_4->addWidget(detect_backB);

        detector_page_rightSP = new QSpacerItem(535, 19, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(detector_page_rightSP);


        gridLayout->addWidget(widget_13, 3, 0, 1, 1);

        stacked_widget->addWidget(detector_page);
        train_page = new QWidget();
        train_page->setObjectName(QStringLiteral("train_page"));
        verticalLayout_3 = new QVBoxLayout(train_page);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        widget = new QWidget(train_page);
        widget->setObjectName(QStringLiteral("widget"));
        horizontalLayout_3 = new QHBoxLayout(widget);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        frame_3 = new QFrame(widget);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(450, 0));
        frame_3->setFrameShape(QFrame::StyledPanel);
        horizontalLayout_7 = new QHBoxLayout(frame_3);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        widget_4 = new QWidget(frame_3);
        widget_4->setObjectName(QStringLiteral("widget_4"));
        verticalLayout_4 = new QVBoxLayout(widget_4);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_2 = new QLabel(widget_4);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_2);

        label = new QLabel(widget_4);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label);

        label_3 = new QLabel(widget_4);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_3);

        label_4 = new QLabel(widget_4);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_4);


        horizontalLayout_7->addWidget(widget_4);

        widget_5 = new QWidget(frame_3);
        widget_5->setObjectName(QStringLiteral("widget_5"));
        verticalLayout_5 = new QVBoxLayout(widget_5);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        widget_7 = new QWidget(widget_5);
        widget_7->setObjectName(QStringLiteral("widget_7"));
        horizontalLayout_5 = new QHBoxLayout(widget_7);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        train_imgsTE = new QLineEdit(widget_7);
        train_imgsTE->setObjectName(QStringLiteral("train_imgsTE"));
        train_imgsTE->setMinimumSize(QSize(30, 30));
        train_imgsTE->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_5->addWidget(train_imgsTE);

        train_imgsB = new QPushButton(widget_7);
        train_imgsB->setObjectName(QStringLiteral("train_imgsB"));
        train_imgsB->setMinimumSize(QSize(30, 30));
        train_imgsB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_5->addWidget(train_imgsB);


        verticalLayout_5->addWidget(widget_7);

        widget_10 = new QWidget(widget_5);
        widget_10->setObjectName(QStringLiteral("widget_10"));
        horizontalLayout_9 = new QHBoxLayout(widget_10);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        valid_imgsTB = new QLineEdit(widget_10);
        valid_imgsTB->setObjectName(QStringLiteral("valid_imgsTB"));
        valid_imgsTB->setMinimumSize(QSize(30, 30));
        valid_imgsTB->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_9->addWidget(valid_imgsTB);

        valid_imgsB = new QPushButton(widget_10);
        valid_imgsB->setObjectName(QStringLiteral("valid_imgsB"));
        valid_imgsB->setMinimumSize(QSize(30, 30));
        valid_imgsB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_9->addWidget(valid_imgsB);


        verticalLayout_5->addWidget(widget_10);

        widget_9 = new QWidget(widget_5);
        widget_9->setObjectName(QStringLiteral("widget_9"));
        horizontalLayout_8 = new QHBoxLayout(widget_9);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        weights_backupTB = new QLineEdit(widget_9);
        weights_backupTB->setObjectName(QStringLiteral("weights_backupTB"));
        weights_backupTB->setMinimumSize(QSize(30, 30));
        weights_backupTB->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_8->addWidget(weights_backupTB);

        weights_backupB = new QPushButton(widget_9);
        weights_backupB->setObjectName(QStringLiteral("weights_backupB"));
        weights_backupB->setMinimumSize(QSize(30, 30));
        weights_backupB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_8->addWidget(weights_backupB);


        verticalLayout_5->addWidget(widget_9);

        widget_8 = new QWidget(widget_5);
        widget_8->setObjectName(QStringLiteral("widget_8"));
        horizontalLayout_6 = new QHBoxLayout(widget_8);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        initial_weightsTB = new QLineEdit(widget_8);
        initial_weightsTB->setObjectName(QStringLiteral("initial_weightsTB"));
        initial_weightsTB->setMinimumSize(QSize(30, 30));
        initial_weightsTB->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_6->addWidget(initial_weightsTB);

        initial_weightsB = new QPushButton(widget_8);
        initial_weightsB->setObjectName(QStringLiteral("initial_weightsB"));
        initial_weightsB->setMinimumSize(QSize(30, 30));
        initial_weightsB->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_6->addWidget(initial_weightsB);


        verticalLayout_5->addWidget(widget_8);


        horizontalLayout_7->addWidget(widget_5);


        horizontalLayout_3->addWidget(frame_3);

        horizontalSpacer_2 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_3->addWidget(widget);

        widget_3 = new QWidget(train_page);
        widget_3->setObjectName(QStringLiteral("widget_3"));
        horizontalLayout_13 = new QHBoxLayout(widget_3);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalSpacer_7 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_7);

        train_summaryTB = new QTextBrowser(widget_3);
        train_summaryTB->setObjectName(QStringLiteral("train_summaryTB"));
        train_summaryTB->setMinimumSize(QSize(320, 0));

        horizontalLayout_13->addWidget(train_summaryTB);

        horizontalSpacer_8 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_8);


        verticalLayout_3->addWidget(widget_3);

        widget_6 = new QWidget(train_page);
        widget_6->setObjectName(QStringLiteral("widget_6"));
        horizontalLayout_11 = new QHBoxLayout(widget_6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_4);

        frame = new QFrame(widget_6);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_12 = new QHBoxLayout(frame);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        train_startB = new QPushButton(frame);
        train_startB->setObjectName(QStringLiteral("train_startB"));
        train_startB->setMinimumSize(QSize(120, 40));
        train_startB->setMaximumSize(QSize(140, 60));

        horizontalLayout_12->addWidget(train_startB);


        horizontalLayout_11->addWidget(frame);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_6);


        verticalLayout_3->addWidget(widget_6);

        widget_2 = new QWidget(train_page);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setMinimumSize(QSize(0, 40));
        widget_2->setMaximumSize(QSize(16777215, 40));
        horizontalLayout_10 = new QHBoxLayout(widget_2);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        train_backB = new QPushButton(widget_2);
        train_backB->setObjectName(QStringLiteral("train_backB"));

        horizontalLayout_10->addWidget(train_backB);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);


        verticalLayout_3->addWidget(widget_2);

        stacked_widget->addWidget(train_page);
        config_page = new QWidget();
        config_page->setObjectName(QStringLiteral("config_page"));
        verticalLayout = new QVBoxLayout(config_page);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        widget_12 = new QWidget(config_page);
        widget_12->setObjectName(QStringLiteral("widget_12"));
        horizontalLayout_2 = new QHBoxLayout(widget_12);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        configPTE = new QPlainTextEdit(widget_12);
        configPTE->setObjectName(QStringLiteral("configPTE"));

        horizontalLayout_2->addWidget(configPTE);


        verticalLayout->addWidget(widget_12);

        widget_11 = new QWidget(config_page);
        widget_11->setObjectName(QStringLiteral("widget_11"));
        widget_11->setMinimumSize(QSize(0, 40));
        widget_11->setMaximumSize(QSize(16777215, 40));
        horizontalLayout_14 = new QHBoxLayout(widget_11);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        config_backB = new QPushButton(widget_11);
        config_backB->setObjectName(QStringLiteral("config_backB"));
        config_backB->setMinimumSize(QSize(0, 0));

        horizontalLayout_14->addWidget(config_backB);

        config_page_rightSP = new QSpacerItem(535, 19, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(config_page_rightSP);

        config_saveB = new QPushButton(widget_11);
        config_saveB->setObjectName(QStringLiteral("config_saveB"));
        config_saveB->setMinimumSize(QSize(0, 0));

        horizontalLayout_14->addWidget(config_saveB);


        verticalLayout->addWidget(widget_11);

        stacked_widget->addWidget(config_page);

        gridLayout_2->addWidget(stacked_widget, 0, 0, 1, 1);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_5);

        label_5 = new QLabel(central_widget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMaximumSize(QSize(181, 61));
        label_5->setPixmap(QPixmap(QString::fromUtf8("logos/32820_276276201881050.jpg")));
        label_5->setScaledContents(true);

        horizontalLayout_16->addWidget(label_5);

        label_6 = new QLabel(central_widget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMaximumSize(QSize(221, 61));
        label_6->setPixmap(QPixmap(QString::fromUtf8("logos/33332_17101710201110155.jpg")));
        label_6->setScaledContents(true);

        horizontalLayout_16->addWidget(label_6);

        label_7 = new QLabel(central_widget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMaximumSize(QSize(161, 61));
        label_7->setPixmap(QPixmap(QString::fromUtf8("logos/logo_Rovimatica.png")));
        label_7->setScaledContents(true);

        horizontalLayout_16->addWidget(label_7);

        label_8 = new QLabel(central_widget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMaximumSize(QSize(61, 61));
        label_8->setPixmap(QPixmap(QString::fromUtf8("logos/virtual_drone_box_icon.jpg")));
        label_8->setScaledContents(true);

        horizontalLayout_16->addWidget(label_8);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_9);


        gridLayout_2->addLayout(horizontalLayout_16, 1, 0, 1, 1);

        MainWindow->setCentralWidget(central_widget);

        retranslateUi(MainWindow);

        stacked_widget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Virtual-Drone-Box", nullptr));
        main_pageGB->setTitle(QApplication::translate("MainWindow", "Main Menu", nullptr));
        main_detectorB->setText(QApplication::translate("MainWindow", "Detector", nullptr));
        main_trainB->setText(QApplication::translate("MainWindow", "Train", nullptr));
        main_configB->setText(QApplication::translate("MainWindow", "Configuration", nullptr));
        bCamera->setText(QString());
        label_9->setText(QApplication::translate("MainWindow", "Input Images", nullptr));
        detector_imgsB->setText(QApplication::translate("MainWindow", "...", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Weights", nullptr));
        detector_weightsB->setText(QApplication::translate("MainWindow", "...", nullptr));
        detector_startB->setText(QApplication::translate("MainWindow", "Start", nullptr));
        detect_backB->setText(QApplication::translate("MainWindow", "Back", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Training Images", nullptr));
        label->setText(QApplication::translate("MainWindow", "Validation Images", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Weights Backup", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Initial Weights", nullptr));
        train_imgsB->setText(QApplication::translate("MainWindow", "...", nullptr));
        valid_imgsB->setText(QApplication::translate("MainWindow", "...", nullptr));
        weights_backupB->setText(QApplication::translate("MainWindow", "...", nullptr));
        initial_weightsB->setText(QApplication::translate("MainWindow", "...", nullptr));
        train_startB->setText(QApplication::translate("MainWindow", "Start", nullptr));
        train_backB->setText(QApplication::translate("MainWindow", "Back", nullptr));
        config_backB->setText(QApplication::translate("MainWindow", "Back", nullptr));
        config_saveB->setText(QApplication::translate("MainWindow", "Save", nullptr));
        label_5->setText(QString());
        label_6->setText(QString());
        label_7->setText(QString());
        label_8->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
