#pragma once

#include <virtual_drone_box/export.h>

#include <QDir>
#include <QThread>

#include <virtual_drone_box/DetectionData.h>

#include <virtual_drone_box/DDetectorThread.h>

#include <virtual_drone_box/ZedCamera.h>

#ifdef _WIN32
#include <virtual_drone_box/MJPEGWriter.h>
#endif

namespace catec
{
class DDetector : public QObject
{
   Q_OBJECT
  public:
   DDetector();
   ~DDetector();

   void setPathToDarknet(const QString &path);
   void setNetworkWeights(const QString &path);
   void setNetworkArch(const QString &path);

   void setInputFolder(const QString &path);
   void setOutputFolder(const QString &path);

   void process();
   //JSG
   void processWithCamera();

   //JSG_16_07_2020
   void stop();
   void stopWithCamera();

   int getImgsCount() const;

   bool isReady() const;

  private slots:
   void handleResults(QList<DetectionData> result_data);
    //JSG_17_07_2020
   void slotEndProcess();
   //Slot para recibir la imagen del streaming
   void slotReceiveMJPEGWriter(cv::Mat img);
  signals:
   void startProcessing(const QString network_arch_path, const QString network_weights_path,
                        const QStringList inputs_paths);
   void resultReady(QList<DetectionData> result_data);
   //JSG_16_07_2020
   void stopProcessing();
   //JSG_17_07_2020
   void signalEndProcess();
   //JSG
   void signalInitializeCamera();
   void signalCloseCamera();
  private:
   QString _darknet_path;
   QString _network_weights_path;
   QString _network_arch_path;
   QString _input_folder_path;
   QString _output_folder_path;

   QDir _darknet_dir;
   QDir _network_weights_dir;
   QDir _network_arch_dir;
   QDir _input_folder_dir;
   QDir _output_folder_dir;

   QStringList _filters;
   QStringList _input_imgs_paths;

   QThread _detector_thread;
   DDetectorThread *_detector;

   zedCamera * _zed;
   QThread * _zedThread;

   MJPEGWriter _mjpeg;
};

}  // namespace catec
