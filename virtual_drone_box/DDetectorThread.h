#pragma once

/* SOCKET THINGS OUT FOR NOW
#include <arpa/inet.h>  
#include <sys/socket.h>
*/
#include <virtual_drone_box/DetectionData.h>
#include <virtual_drone_box/SystemSettings.h>
#include <virtual_drone_box/export.h>
#include <QObject>
#include <virtual_drone_box/darknet/yolo_v2_class.hpp>
#include "virtual_drone_box/tracker.h"



namespace catec {
class DDetectorThread : public QObject
{
   Q_OBJECT
  public:
   DDetectorThread();
   ~DDetectorThread();

   void setNetworkPath(const QString network_arch_path, const QString network_weights_path);
  public slots:
   void process(const QString network_arch_path, const QString network_weights_path, const QStringList inputs_paths);
   //JSG
   //Método para procesar un único frame
   void processSingleFrame(cv::Mat img);
   //JSG_16_07_2020
   void stop();

  signals:
   void resultReady(QList<DetectionData> result_data);
   //JSG_17_07_2020
   void signalSendEndProcess ();
   //Señal para enviar imagen al streaming mjpeg
   void sendSignalMJPEGWriter(cv::Mat img);
  private:
   double stepTime, iou_weight;
   int minConsFrames;
   int maxFrames;
   int maxDistance;
   int maxSize;
   int port;
   QString host;

   Detector *_detector;

   //JSG_16_07_2020
   bool _isStarted;

   void updateImages(cv::Mat &img, std::deque<tracker> tracks);
   void trackinfo(std::deque<tracker> tracks);
   //int connect(int sock, sockaddr_in serv_addr); //SOCKET THINGS OUT FOR NOW
   std::deque<tracker> _tracks;
   //MJPEGWriter _mjpeg;
};

}  // namespace catec
