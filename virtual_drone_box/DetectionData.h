#pragma once

#include <QFileInfo>
#include <QList>
#include <QMetaType>

#include <virtual_drone_box/darknet/yolo_v2_class.hpp>

class DetectionData
{
  public:
   DetectionData(){};
   ~DetectionData(){};

   QFileInfo img_file;
   QList<bbox_t> detections;
};

Q_DECLARE_METATYPE(DetectionData)
Q_DECLARE_METATYPE(QList<DetectionData>)
