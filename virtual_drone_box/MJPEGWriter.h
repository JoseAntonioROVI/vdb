#include <QDebug>
#include <QObject>

#include <QTcpServer>
#include <QTcpSocket>

#include <opencv2/opencv.hpp>

//***************************************************************************************
//Servidor de streaming mejorado por ROVIMATICA.
//***************************************************************************************

class MJPEGWriter : public QObject
{
    Q_OBJECT

private:

    int quality; // jpeg compression [1..100]
    cv::Mat lastFrame;
    int port;

    struct customSocket
    {
        QTcpSocket * socket;
        bool readyToSendImages;
    };
    QTcpServer * _server;
    QList<customSocket> _listClients;
public:

    MJPEGWriter(int port = 0);
    ~MJPEGWriter();

    void start();
    void stop(bool isStopped);

    void setPort (int port);



private:
    //Método para inicializar el servidor
    void startServer();
    //Bool para controlar si el servidor está parado o no
    bool _isStopped;

signals:
    void sendAddLogInfo(QString info);
public slots:
    void write(cv::Mat frame);

    void newServerConnection();
    void readyRead();
    void disconnectedClient();
    //Este slot es el que va a preparar para enviar las imágenes a cada uno de los clientes
    void manageImagesToClients();
    //Método para enviar la imagen a un cliente
    //void sendImageToClient(QTcpSocket * socket, std::vector<uchar> buffer);

};

