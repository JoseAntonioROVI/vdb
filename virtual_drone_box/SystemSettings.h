#pragma once

#include <virtual_drone_box/export.h>

#include <QSettings>

namespace catec
{
class SystemSettings
{
  public:
   static SystemSettings* getInstance();

   QSettings* settings;

  protected:
   SystemSettings(){};
   static SystemSettings* instance;
};
}  // namespace catec
