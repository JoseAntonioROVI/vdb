#pragma once

#include <virtual_drone_box/export.h>

#include <QDir>

namespace catec
{
class Train
{
  public:
   enum FilesType
   {
      TRAINING,
      VALIDATION
   };

   Train();
   ~Train();

   void setPathToDarknet(const QString &path);

   void setTrainImgs(const QString &path);
   void setValidImgs(const QString &path);
   void setBackupPath(const QString &path);
   void setInitialWeights(const QString &path);
   void setNetworkArch(const QString &path);

   void train();
   void stop();

   bool isReady() const;

   int getFilesNumber(FilesType type) const;

  private:
   void createImgsTxtFile(FilesType type);
   void createObjFiles();

   QString _darknet_path;
   QString _train_imgs_path;
   QString _valid_imgs_path;
   QString _backup_path;
   QString _initial_weights_path;
   QString _network_arch_path;

   QDir _darknet_dir;
   QDir _train_imgs_dir;
   QDir _valid_imgs_dir;
   QDir _backup_dir;
   QDir _initial_weights_dir;
   QDir _network_arch_dir;

   QStringList _filters;

   int _train_imgs_count = 0;
   int _valid_imgs_count = 0;
};

}  // namespace catec
