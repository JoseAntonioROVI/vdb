#ifndef ZEDCAMERA_H
#define ZEDCAMERA_H

#include <QObject>
#include <QTimer>
//Zed camera
#include <sl/Camera.hpp>
//OpenCV
#include <opencv2/opencv.hpp>

#define FPS_ZED 15


class zedCamera : public QObject
{
    Q_OBJECT
public:
    explicit zedCamera(QObject *parent = nullptr);
    ~zedCamera();

signals:
    void sendImageCV(const cv::Mat &cvImLeft);
    void signalEndInit();
public slots:
    void initialize();
    void closeConnection();
    void capture();
    //void slotStartStopTimer();
private:
    sl::Camera _zedCam;
    sl::InitParameters _initParametersZed;

    QTimer * _timerGrabImage;

    cv::Mat slMat2cvMat (sl::Mat & input);
};

#endif // ZEDCAMERA_H
