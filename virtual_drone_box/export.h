
#ifndef VIRTUAL_DRONE_BOX_EXPORT_H
#define VIRTUAL_DRONE_BOX_EXPORT_H

#ifdef VIRTUAL_DRONE_BOX_STATIC_DEFINE
#  define VIRTUAL_DRONE_BOX_EXPORT
#  define VIRTUAL_DRONE_BOX_NO_EXPORT
#else
#  ifndef VIRTUAL_DRONE_BOX_EXPORT
#    ifdef virtual_drone_box_EXPORTS
        /* We are building this library */
#      define VIRTUAL_DRONE_BOX_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define VIRTUAL_DRONE_BOX_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef VIRTUAL_DRONE_BOX_NO_EXPORT
#    define VIRTUAL_DRONE_BOX_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef VIRTUAL_DRONE_BOX_DEPRECATED
#  define VIRTUAL_DRONE_BOX_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef VIRTUAL_DRONE_BOX_DEPRECATED_EXPORT
#  define VIRTUAL_DRONE_BOX_DEPRECATED_EXPORT VIRTUAL_DRONE_BOX_EXPORT VIRTUAL_DRONE_BOX_DEPRECATED
#endif

#ifndef VIRTUAL_DRONE_BOX_DEPRECATED_NO_EXPORT
#  define VIRTUAL_DRONE_BOX_DEPRECATED_NO_EXPORT VIRTUAL_DRONE_BOX_NO_EXPORT VIRTUAL_DRONE_BOX_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef VIRTUAL_DRONE_BOX_NO_DEPRECATED
#    define VIRTUAL_DRONE_BOX_NO_DEPRECATED
#  endif
#endif

#define DEBUG 1

#endif
