#include <iostream>
#include "Eigen/Dense"
//#include <Eigen/Dense>
//#include <Dense>
#pragma once
//#define nvar 4

//using namespace catec;
//using Eigen::MatrixXd;

class kalman
{
  public:
   kalman(double time_step = 0.04);

   double filtered_x, filtered_y, filtered_w, filtered_h;  // x,y output coordinates

   void predict();
   void update(bool isdata);
   void iterate(double x, double y, double width, double height, bool isdata = true);
   Eigen::VectorXd xk1, xk;  // xk1 is ^x(k-1)  (estimated current state) & xk is the prediction of the next state
   double vx, vy, w, h;
   double input_x, input_y;

  private:
   Eigen::MatrixXd F, H, Q, R, P, K;  // P = estimated state covariance & K = Kalman gain
   double dt;                         // Step time, current time, and initial time
   Eigen::VectorXd z;                 // measurement vector
   bool ini;
};
