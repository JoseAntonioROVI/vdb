#include <iostream>
#include <string>
#include "kalman.h"
#include <virtual_drone_box/darknet/yolo_v2_class.hpp>

#pragma once

class tracker
{
  public:
   tracker(bbox_t bbox, unsigned int max_skippedFrames = 15, double squared_max_Distance = 150,
           double time_step = 0.04);
   void assignTrack(tracker trackToAssign);
   void doKalman();
   void ider(int nid);
   void addtray(double x, double y);
   std::vector<double> predict();
   double iou(tracker trackToCompare);

   double cx, cy, vx, vy, vmod2;  // centroids && vel && squared vel module
   unsigned int tlx, tly, brx, bry, h, w;

   unsigned int skippedFrames, max_skippedFrames, consecutiveFrames;

   double min_cost, min_dist;  // Distance to the nearest previous track
   std::vector<int> min_cost_tracks;
   int min_cost_track;                  // Index of nearest previous track
   bool assigned, rmv, false_positive;  // Assigned to new track, to be removed, its just a false detection

   std::string id;
   int id_number;

   struct coord
   {
      double x;
      double y;
   };
   std::deque<struct coord> tray;
   std::deque<struct coord> elderV;

  private:
   bool ussingKalman;
   kalman usedKalman;
   double time_step;
};
